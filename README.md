# README #

# Synopsis

This project calculates the billing using electric consumption, demand data and rates rules given by
the electricity provider.

#Rules Directory

By default the rules are saved in directory named rules_directory(auto generated in project folder). Each rule file corresponds to
the name of the electricity provider.

Rules are python dictionary objects.

For eg:
rule1 = {
    "value": 10, "charge_type": "consumption", "electricity_provider": "coned", "rate_unit": "kwh", "location": "nyc",
    "start_date": "12/01/2017", "end_date": "12/30/2017", "start_time": "08:00", "end_time": "22:00",
    "days_applicable": ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
    "floor_limit": 0, "ceiling_limit": 5000, "calculated_on": "measurement", "tag": "peak"
    }


# Instruction

1. Download data in csv format in the project root directory
2. Sort and update data for the "template file"
3. Sort and update data for the "values file"
4. In the load_data_pkl.py file, update the load_rules function's template_file and values_file parameter"
5. In the statement_builder.py file, update the generate_statement_number function to return the desired invoice number
6. Uncomment the code block for the entity that you would wish to run for in the xxx.py file
7. In the xxx.py file, update the start_date and end_date for it to reflect the dates from the "template_file" being used
8. Run the xxx.py file
9. to view output...go into the terminal and put in command 'open ~/.rates_engine' and the jsons will be put in the relevant company