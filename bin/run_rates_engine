#!usr/bin/env python
# coding=utf-8

import warnings

warnings.simplefilter('ignore')
import argparse

from rates_engine.main import generate_report

parser = argparse.ArgumentParser()

parser.add_argument('company', type=str, help='Company name')

parser.add_argument('building', type=str, help='Building id')

parser.add_argument('entity', type=str,
                    help='Name of the billing entity')

parser.add_argument('parent_entity', type=str,
                    help='Name of the entity who is going to charge')

parser.add_argument('start_date', type=str,
                    help='Start date of the billing period eg: 2017-09-01')

parser.add_argument('end_date', type=str,
                    help='End date of the billing period eg: 2017-09-30')

parser.add_argument('schedule', type=str,
                    help='rates schedule for the building')

parser.add_argument('zipcode', type=int, help='Zip code for the building'
                                              'area.')

parser.add_argument('--consumption_granularity', type=str, default='5min',
                    help='Consumption data granularity.')

parser.add_argument('--statement_type', type=str, default='ar',
                    help='type of statement(eg: ap or ar), default: ar.')

parser.add_argument('--demand_granularity', type=str, default='5min',
                    help='Demand data granularity.')

parser.add_argument('--use_beta', type=bool, default=False,
                    help='Whether to use dev env or not default False')

args = parser.parse_args()

generate_report(
    company=args.company,
    building=args.building,
    entity=args.entity,
    parent_entity=args.parent_entity,
    start_date=args.start_date,
    end_date=args.end_date,
    zipcode=args.zipcode,
    schedule=args.schedule,
    consumption_gran=args.consumption_granularity,
    demand_gran=args.demand_granularity,
    statement_type=args.statement_type,
    use_beta=args.use_beta)
