
import logging
from datetime import timedelta

import pandas as pd
import numpy as np

from an_common.resource import get_cumulative_consumption_ts, get_readings_df,\
        get_resource_doc

from .statement_builder import get_service_type

logger = logging.getLogger(__name__)


def get_consumption_data(
        company, building, start_date, end_date, statement_type, include_data,
        time_zone, cache, reading_gran, aggregate,
        device_id_list,
        resource
):

    """
    Returns the consumption data for given date range and granularity.

    :param company: string, name of the company, which owns the building.
    :param building: string, name of the building.
    :param start_date: string, billing start date.
    :param end_date: string, billing end date.
    :param include_data: tuple,e options:
                                gte -> greater than equal to the start date
                                lte-> less than or equal to the end date
                                gt -> only greater than start date
                                lt-> only less than end date
                        default=(gte, lte)
    :param time_zone: timezone to fetch the data
    :param cache: boolean, whether to store a local copy of data or not.
    :param reading_gran: string eg: 15min, data reading frequency.
    :param device_id_list: list, list of device ids to be included.
    :param aggregate: boolean,sum sensors data for cumulative consumption
    :param resource: string,
        Name of the resource eg: electric_consumption, steam_consumption etc.
        Default is electric_consumption.
    :return: pd.timeseries.
    """

    try:
        start_date_modified = (pd.to_datetime(start_date) - timedelta(days=1)).strftime('%Y-%m-%d')
        end_date_modified = (pd.to_datetime(end_date) + timedelta(days=1)).strftime(
            '%Y-%m-%d')
        consumption_data = get_cumulative_consumption_ts(
            company=company, building=building,
            resource=resource, date=(start_date_modified, end_date_modified),
            date_mode=include_data, tz=time_zone, cache=cache,
            readings_gran=reading_gran, select_by_id=device_id_list,
            aggregate=aggregate)
        if time_zone is not None:
            consumption_data = consumption_data.tz_convert(time_zone)
            consumption_data = consumption_data[start_date: end_date]
        if resource == 'electric_consumption_clean':
            consumption_data = consumption_data[[col for col in consumption_data.columns if col not in ['missing', 'out_of_bounds', 'semantically_incorrect']]]
        return consumption_data
    except Exception as e:
        print(e)
        return None


def get_demand_data(
        company, building, start_date, end_date, statement_type, include_data,
        time_zone, cache, reading_gran, aggregate,
        device_id_list, resource):

    """
    Returns the cumulative consumption data for given date range and
    granularity.

    :param company: string, name of the company, which owns the building.
    :param building: string , name of the building.
    :param start_date: string, billing start date.
    :param end_date: string, billing end date.
    :param include_data: tuple,e options:
                                gte -> greater than equal to the start date
                                lte-> less than or equal to the end date
                                gt -> only greater than start date
                                lt-> only less than end date
                        default=(gte, lte)
    :param time_zone: timezone to fetch the data
    :param cache: boolean, whether to store a local copy of data or not.
    :param reading_gran: string eg: 15min, frequency to read data.
    :param aggregate: boolean, if True return aggregate demand
    :param device_id_list: list, list of device ids to be included.
    :param resource: string,
            Name of the resource, eg: electric_demand
    :return: pd.timeseries.
    """

    try:
        demand_data = get_readings_df(
            company=company, building=building, resource=resource,
            date=(start_date, end_date), date_mode=include_data,
            cache=cache, gran=reading_gran, select_by_id=device_id_list)
        demand_data = pd.DataFrame(demand_data)
    except Exception as e:
        print(e)
        return None
    if time_zone is not None:
        demand_data = demand_data.tz_convert(time_zone)
    if aggregate:
        return demand_data.sum(axis=1)
    else:
        return demand_data


def get_billing_period(company, building, use_beta, start_date, end_date):
    """
    Return period id for given period id.

    :param use_beta: boolean, whether to use dev environment or not.
    :param building: string, namespace of the building
    :param company: string, name of the company.
    :param start_date: string, start date of billing period
    :param end_date: string, end date of billing period
    :return: string, period id of the billing period.
    """
    # print('start_date', start_date)
    # print('end_date', end_date)
    billing_period = get_resource_doc(
        company=company,
        building=building,
        resource='billing_periods',
        query={"start_date": start_date, "end_date": end_date, "service_type": get_service_type()},
        extract_all=True, use_beta=use_beta, domain='billing'
    )[0]
    # print('billing_period', billing_period)
    period_id = billing_period['_id']
    return period_id


def check_the_consumption(x):
    try:
        if np.diff(x) < 0:
            return float('NaN')
        else:
            return np.diff(x)
    except:
        return 0

def filter_billing_accounts(accounts, account_id):
    filtered_accounts = []

    for account in accounts:
        if account["service_type"] == get_service_type() and account["_id"] == account_id:
            filtered_accounts.append(account)

    return filtered_accounts
