import pandas as pd

def literal(x):
    literal_list = []
    if not pd.isna(x):
        x = x[1:-1]
        x = x.split(',')
        for i in x:
            try:
                literal_list.append(int(i))
            except ValueError:
                literal_list.append(i)
    return literal_list

def fix_numeric(value):
    if isinstance(value, str) and value.startswith('$'):
        return float(value[1:])
    if isinstance(value, str):
        if '.' in value:
            return float(value)
        return int(value)
    return value

def fix_values_column_headers(template, values):
    for key in values.keys():
        str_key = str(key)
        if str_key != str_key.strip():
            values = values.rename({str_key: str_key.strip()}, axis=1)

    for index, row in template.iterrows():
        data_name = row['Unnamed: 0'].strip()
        data_letter = row['id']
        if data_name not in values:
            raise 'Missing ' + data_name + ' column in values'
        values = values.rename({data_name: data_letter}, axis=1)

    return values

def create_rules_csv(template_file, values_file):
    template = pd.read_csv(template_file, header='infer')
    template = template.rename({'_id': 'id'}, axis=1)
    template['parent_rules'] = template['parent_rules'].apply(lambda x: literal(x))
    template['days_applicable'] = template['days_applicable'].apply(lambda x: literal(x))
    template['months_applicable'] = template['months_applicable'].apply(lambda x: literal(x))

    values = pd.read_csv(values_file, header='infer')
    values = fix_values_column_headers(template, values)

    try:
        template = template.drop('Unnamed: 0', axis=1)
    except:
        pass

    rules_list = []
    for index, row in template.iterrows():
        for value_index, value_row in values.iterrows() :
            if not pd.isna(value_row[row['id']]):
                new_row = row.copy()
                new_row['value'] = fix_numeric(value_row[row['id']])
                new_row['entity_id'] = value_row['account_id']
                rules_list.append(new_row)
            else:
                new_row = row.copy()
                new_row['value'] = 0
                new_row['entity_id'] = value_row['account_id']
                rules_list.append(new_row)
    rules=pd.DataFrame(rules_list)
    rules['tax_type'] = 'Sales Tax'
    rules['c_or_d'] = 'd'
    return rules


def load_rules(template_file, values_file):
    """
    Loads the rules for given provider which falls in given date time range.

    :param provider: string, name of the serivce provider
    :param start_date: string, start_date of the rule
    :param end_date: string, end date of rule
    :return: pd.dataframe, dataframe of rules
    """
    df = create_rules_csv(template_file, values_file)
    df = df[df['value'].astype('float') > 0]
    df.loc[(df.end_date == 'None'), 'end_date'] = pd.to_datetime(
        'today').strftime('%Y-%m-%d')
    df['days_applicable'] = df['days_applicable'].apply(lambda x: [int(i) for i in x])
    df['months_applicable'] = df['months_applicable'].apply(
        lambda x: [int(i) for i in x])
    df['location'] = df['location'].apply(lambda x: x[1:-1])
    df['location'] = df['location'].apply(lambda x: [x])
    return df


# def build_rule_entries():
#     rules = load_rules(provider='rules_nagra_June.csv')
#     for index, rule in rules.iterrows():
#         if len(rule['parent_rules']) < 1:
#             rules.loc[index, 'charge_entry'] = rule['charge_type']
#         else:
#
#             for i in rule['parent_rules']:
#                 rules.loc[index, 'charge_entry' +'_'+str(i)] = ''
#                 if len(rules.loc[index, 'charge_entry'+'_'+str(i)]) < 1:
#                     rules.loc[index, 'charge_entry' +'_'+str(i)] += rules[rules._id == i].iloc[0]['charge_type']
#                 else:
#                     rules.loc[index, 'charge_entry'+'_'+str(i)] += ' > ' + \
#                     rules[rules._id == i].iloc[0]['charge_entry']
#                 rules.loc[index, 'charge_entry'+'_'+str(i)] +=' > '+ rules.loc[index, 'charge_type']
#
#     return rules

