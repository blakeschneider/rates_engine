import logging
from datetime import timedelta
import pandas as pd
import numpy as np
from an_common.api import get_resource_doc

from rates_engine.utils import get_holiday_list, \
    prepare_measurement_description, is_on_peak, save_report, \
    merge_off_peak_measurements, update_charges_with_merged_measurement,\
    find_distinct_meters
from rates_engine.data_handler import get_consumption_data, get_demand_data,\
                                get_billing_period, check_the_consumption, filter_billing_accounts
from rates_engine.rules_handler import filter_rules, classify_rules,\
    process_consumption_rules, process_demand_rules,\
    process_measurement_children, process_subtotal_child_rules,\
    filter_demand_rules_with_ceil_and_floor, group_and_sort_demand_rules,\
    process_demand_groups, process_cumulative_rules, preprocess_ar_rules,\
    build_rule_entries

from an_common.collection import get_billing_account, get_billing_meters

from .statement_builder import prepare_statement, get_service_type, get_consumption_service_type, get_demand_service_type

from .post_process_statement import remove_duplicate_measurements, update_measurement_type_wording, update_group_name

from an_common.resource import get_building_metadata
from .load_data_pkl import load_rules

logger = logging.getLogger(__name__)


def generate_report_helper(
        rules, start_date, end_date, data, mode, meter_id, meter_multiplier,
        calculate_consumption, on_peak_start,meter_correction_factor=None):

    """
    This is a main pipeline function, which loads demand and consumption data
    for given building name, rules for the provider and uses both of them to
    generate the billing report for the given time period.

    :param rules: list of pd.DataFrame,
        List as [demand_with_ceil_rules, normal_rules]
    :param data: pd.DataFrame, demand or consumption data.
    :param mode: string
        Calculation type (consumption_from_consumption or
        consumption_from_demand)
    :param meter_id: string, meter identifier
    :param meter_multiplier: float, meter correction factor
    :param start_date: string, start date of the bill
    :param end_date: string, end date of the bill.
    :return: report
    """
    holidays_list = get_holiday_list(start_date=start_date, end_date=end_date)

    total_charges = 0
    pointer_map = {}
    measurement_dict = {}
    subtotal_dict = {}
    measurement_dependent_children = list()
    subtotal_dependent_children = list()
    report_cache = list()
    measurement_entries = list()
    demand_with_ceil = None
    normal_rules = rules
    group_type_cache = {}
    measurement_uuid_cache = {}
    if mode == 'demand':
        demand_with_ceil = rules[0]
        normal_rules = rules[1]
    if demand_with_ceil is not None:
        group_charges, measurement_entry = process_demand_groups(
            demand_groups=demand_with_ceil, demand_data=data,
            pointer_map=pointer_map, subtotal_dict=subtotal_dict,
            measurement_dict=measurement_dict,
            holidays_list=holidays_list, report_cache=report_cache,
            start_date=start_date, end_date=end_date,
            meter_id=meter_id, meter_multiplier=meter_multiplier)
        total_charges += group_charges
        if len(measurement_entry) > 0:
            measurement_entries += measurement_entry
    for _, rule in normal_rules.iterrows():

        # Check for the rules applicable to measurements.
        if rule['calculated_on'].lower() == 'measurement':
            if len(rule['parent_rules']) > 0:
                measurement_dependent_children.append(rule)
                continue
            # apply the rule to consumption data
            if rule['rate_unit'].split('/')[1].lower() == 'kbtu' or rule['rate_unit'].split('/')[1].lower() == 'kwh':

                if on_peak_start is not None:
                    if is_on_peak(rule):
                        rule['start_time'] = on_peak_start
                    elif 5 not in rule['days_applicable'] and \
                            6 not in rule['days_applicable']:
                        rule['end_time'] = on_peak_start
                        rule['end_time'] = pd.to_datetime(rule['end_time'])
                        rule['end_time'] = rule['end_time'] - timedelta(minutes=5)
                        rule['end_time'] = rule['end_time'].time().strftime('%H:%M')

                rule['peak_description'] = prepare_measurement_description(
                    rule['charge_type'], measurement_type='consumption')
                if mode.lower() == 'consumption':
                    group_type_cache[rule['_id']] = rule['rate_unit'].lower()
                    rule_charges, rule_measurements = \
                        process_consumption_rules(
                            rule=rule, consumption_data=data,
                            subtotal_dict=subtotal_dict,
                            measurement_dict=measurement_dict,
                            pointer_map=pointer_map,
                            holidays_list=holidays_list,
                            report_cache=report_cache, start_date=start_date,
                            end_date=end_date, meter_id=meter_id,
                            meter_multiplier=meter_multiplier,
                            measurement_uuid_cache=measurement_uuid_cache,
                            measurement_entries_list=measurement_entries,
                            calculation_type='consumption_from_consumption')
                    total_charges += rule_charges
                    if rule_measurements is not None and len(rule_measurements) > 0:
                        measurement_entries.append(rule_measurements)
                elif calculate_consumption:
                    rule_charges, rule_measurements =\
                        process_consumption_rules(
                            rule=rule, consumption_data=data,
                            subtotal_dict=subtotal_dict,
                            measurement_dict=measurement_dict,
                            pointer_map=pointer_map,
                            holidays_list=holidays_list,
                            report_cache=report_cache,
                            start_date=start_date, end_date=end_date,
                            meter_id=meter_id,
                            meter_multiplier=meter_multiplier,
                            measurement_uuid_cache=measurement_uuid_cache,
                            measurement_entries_list=measurement_entries,
                            calculation_type='consumption_from_demand')
                    print('rule_charges', rule_charges)
                    # total_charges += rule_charges
                    if rule_measurements is not None and len(rule_measurements) > 0:
                        measurement_entries.append(rule_measurements)
                        total_charges += rule_charges
            # apply the rule to demand data
            # print('mode is ', mode, measurement_entries)
            if mode == 'demand' and rule['rate_unit'].split('/')[1].lower() == 'kw':
                group_type_cache[rule['_id']] = rule['rate_unit'].lower()
                rule['peak_description'] = prepare_measurement_description(
                    rule['charge_type'], measurement_type='demand')
                rule_charges, rule_measurements = process_demand_rules(
                    rule=rule, demand_data=data,
                    demand_definition=rule['time_window'],
                    pointer_map=pointer_map, subtotal_dict=subtotal_dict,
                    measurement_dict=measurement_dict,
                    holidays_list=holidays_list, report_cache=report_cache,
                    start_date=start_date, end_date=end_date,
                    meter_id=meter_id, meter_multiplier=meter_multiplier,
                    measurement_uuid_cache=measurement_uuid_cache,
                    measurement_entries_list=measurement_entries,
                    meter_correction_factor=meter_correction_factor
                )
                # total_charges += rule_charges
                if len(rule_measurements) != 0:
                    measurement_entries.append(rule_measurements)
                    total_charges += rule_charges
        # check for the rules applicable to subtotal
        if rule['calculated_on'].lower() == 'subtotal' and \
                rule['parent_rules']:
                subtotal_dependent_children.append(rule)
                continue
    logger.info('Processing child rules.')
    if measurement_dependent_children:
        total_charges += process_measurement_children(
            rules=measurement_dependent_children,
            measurement_dict=measurement_dict,
            report_cache=report_cache, start_date=start_date,
            end_date=end_date, meter_id=meter_id,
            group_type_cache=group_type_cache, subtotal_dict=subtotal_dict,
            measurement_uuid_cache=measurement_uuid_cache)
    charges = 0
    if subtotal_dependent_children:
        charges = process_subtotal_child_rules(
            subtotal_rules=subtotal_dependent_children,
            subtotal_dict=subtotal_dict, report_cache=report_cache,
            start_date=start_date, end_date=end_date, meter_id=meter_id,
            group_type_cache=group_type_cache,
            measurement_uuid_cache=measurement_uuid_cache)
    total_charges += charges
    if len(measurement_entries) > 0:
        measurement_entries, deleted_uuids, new_uuid = \
                                merge_off_peak_measurements(measurement_entries)
        if len(deleted_uuids) > 0:
            report_cache = update_charges_with_merged_measurement(
                    charge_report=report_cache,
                    deleted_uuids=deleted_uuids,
                    new_uuid=new_uuid)
    # print(total_charges)
    return total_charges, report_cache, measurement_entries


def get_sensor_by_id(company, building, sensor_id):
    sensors_query = {'query': {"_id": sensor_id}}
    return get_resource_doc(company=company, building=building, resource='sensors', query=sensors_query)


def upgrade_billing_meter(company, building, billing_meter):
    sensor = get_sensor_by_id(company, building, billing_meter['sensor_id'])
    billing_meter['sensor'] = sensor
    return billing_meter


def get_billing_meters_by_resource(billing_meters, service_type):
    result = {}
    for billing_meter in billing_meters:
        if billing_meter['service_type'] != service_type:
            continue
        resource = billing_meter['sensor']['resource']
        if resource not in result:
            result[resource] = []
        result[resource].append(billing_meter)

    return result


def filter_out_junk_meters(billing_meters, billing_period_start_date):
    result = []
    for billing_meter in billing_meters:
        if billing_meter['sensor_id'] in ['5f24642683409fae32181a6d', '5f6ce139976023073187067f']:
            continue
        if 'meter_end_date' in billing_meter and billing_meter['meter_end_date'] < billing_period_start_date:
            continue
        result.append(billing_meter)
    return result


def generate_report(company, building, entity_name, entity_id, account_id, statement_number_prefix, start_date, end_date,
                    schedule, statement_type, consumption_gran='1min',
                    demand_gran=None, cache=False, use_beta=False, template_file=None, values_file=None):
    """
    This is a main pipeline function, which loads demand and consumption data
    for given building name, rules for the provider and uses both of them to
    generate the billing report for the given time period.

    :param company: string, name of the company which own the building (eg:
                            rudin)
    :param building: string, id of the building.
    :param provider: string, name of the electricity provider
    :param start_date: string, start date of the bill
    :param end_date: string, end date of the bill.
    :param zipcode: string, zipcode of the building area.
    :param schedule: string, power schedule applicable to the building
    :param consumption_gran: string, freq to read consumption data(eg: 30min)
    :param demand_gran: string, freq to read demand data(eg: 1min)
    :param cache: boolean, cache data for future calculations.
    :param use_beta: boolean, True if want to run against dev environment.
    :return: report
    """

    billing_reports = []
    total_charges = 0
    tz = get_building_metadata(company, building, 'timezone')
    print('\ngenerating', get_service_type(), 'report for entity', entity_name, start_date, end_date)
    if entity_id is None:
        logger.info('Entity id not found for %s', entity_name,
                    ' . Stopping billing process now.')
        return []
    billing_accounts = get_billing_account(
        company=company, building=building, entity_id=entity_id,
        use_beta=use_beta)

    billing_accounts = filter_billing_accounts(billing_accounts, account_id)

    if len(billing_accounts) != 1:
        logger.info('No billing account found for %s', entity_name,
                    ' . Stopping billing process now.')
        return []

    period_id = get_billing_period(
                company=company, building=building, use_beta=use_beta,
                start_date=start_date,
                end_date=end_date)

    print('processing billing account', billing_accounts[0], 'period', period_id)
    for account in billing_accounts:
        charges_list = list()
        subtotal_dict = {}
        cumulative_report_cache = list()
        consumption_measurements = list()
        demand_measurements = list()
        consumption_report = list()
        demand_report = list()
        use_consumption = True
        grouped_demand_rules = None
        billing_report = []
        measurements_list = []

        billing_meters = get_billing_meters(
            company=company, building=building, account_id=account['_id'],
            use_beta=use_beta)

        billing_meters = filter_out_junk_meters(billing_meters, start_date)
        # print('account_id', account['_id'])
        # print('billing_meters', billing_meters)
        if billing_meters is None:
            logger.info('No meter found in the building.'
                        ' Stopping billing process now.')
            continue
        if 'on_peak_start_time' in account.keys():
            on_peak_start_time = account['on_peak_start_time']
        else:
            on_peak_start_time = None

        rules = load_rules(template_file, values_file)
        # print('rules1', rules)
        rules['_id'] = rules.id ###'5dd9a83d1e8c5d2433a5c6e6']
        rules = rules[rules['entity_id'] == account['_id']]
        rules['parent_rules'] = rules['parent_rules'].apply(lambda x: [i for i in x if i in rules['_id'].values])
        rules.index = rules['_id'].values
        rules = rules.drop_duplicates(subset='_id')
        # rules = get_billing_rules(company=company, entity_id=parent_id,
        #                           zipcode=zipcode, schedule=schedule)
        #
        # rules = pd.DataFrame.from_dict(rules)
        rules = build_rule_entries(rules)
        # print('rules2', rules)
        rules = filter_rules(rules, start_date, end_date)
        rules = rules.sort_values('floor_limit')
        # print('rules', rules)
        submeter_rules, cumulative_rules = classify_rules(rules)
        normal_rules = submeter_rules
        if statement_type == 'ar':
            normal_rules = preprocess_ar_rules(normal_rules, account)

        logger.info('start consumption data loading.')

        billing_meters = [upgrade_billing_meter(company, building, billing_meter) for billing_meter in billing_meters]

        consumption_data = None
        consumption_billing_meters_by_resource = get_billing_meters_by_resource(billing_meters, get_consumption_service_type())
        consumption_datas = []

        for consumption_resource in consumption_billing_meters_by_resource:
            device_id_list = [x['sensor']['_id'] for x in consumption_billing_meters_by_resource[consumption_resource]]
            consumption_data = get_consumption_data(
                        company=company, building=building, start_date=start_date,
                        end_date=end_date, include_data=('gte', 'lte'), time_zone=tz,
                        cache=cache, reading_gran=consumption_gran,
                        device_id_list=device_id_list, aggregate=False,
                        resource=consumption_resource,
                        statement_type=statement_type)
            consumption_datas.append(consumption_data)

        if consumption_datas:
            consumption_data = pd.concat(consumption_datas, axis=1)

        # consumption_data = consumption_data.interpolate('linear').bfill().ffill()
        # print('consumption_data', consumption_data.columns)
        # print('consumption_data', consumption_data)
        if consumption_data is not None:
            data = consumption_data.rolling(2).apply(
                lambda x: check_the_consumption(x)).cumsum().interpolate(
                method='polynomial', order=1)
            # data = consumption_data
            consumption_data = data + consumption_data.iloc[0]
            consumption_data = consumption_data.interpolate(method='linear').ffill().bfill()
        else:
            use_consumption = False
        logger.info('consumption data loading completed.')
        logger.info('start demand data loading.')

        demand_data = None
        demand_billing_meters_by_resource = get_billing_meters_by_resource(billing_meters, get_demand_service_type())
        demand_datas = []

        for demand_resource in demand_billing_meters_by_resource:
            device_id_list = [x['sensor']['_id'] for x in demand_billing_meters_by_resource[demand_resource]]
            demand_data = get_demand_data(
                company=company, building=building, start_date=start_date,
                end_date=end_date, include_data=('gte', 'lte'), time_zone=tz,
                cache=cache, reading_gran=demand_gran, aggregate=False,
                resource=demand_resource,
                device_id_list=device_id_list, statement_type=statement_type)
            demand_datas.append(demand_data)

        if demand_datas:
            demand_data = pd.concat(demand_datas, axis=1)

        # demand_data = demand_data.loc[:, ~demand_data.columns.duplicated()]
        # print('demand_data', demand_data.columns)
        # print('demand_data', demand_data)
        if demand_data is None:
            use_consumption = True
            # demand_data = consumption_data.resample('15min').mean().diff()
            logger.info('No demand data found. '
                        'Setting demand calculation rules to null')

        else:
            demand_data = demand_data.fillna(method='ffill')
            demand_ceil_rules, normal_rules = \
                filter_demand_rules_with_ceil_and_floor(submeter_rules)
            grouped_demand_rules = group_and_sort_demand_rules(
                demand_ceil_rules)
            logger.info('demand data loading completed.')
        if consumption_data is None and demand_data is None:
            continue
        for meter_index, meter in enumerate(billing_meters):
            print('processing billing meter', meter_index + 1, '/', len(billing_meters), meter['sensor']['resource'], meter)

            # if meter['sensor_id'] in ['5c5de7369bb7853e24c4d187', '5c5de7369bb7853e24c4d17b',
            #                           '5c5de7369bb7853e24c4d225', '5c5de7369bb7853e24c4d1a0']:
            #     continue
            # if meter['_id'] in ['5c796ebb9bb7853e24d9708f', '5c79718b9bb7853e24dc9b2d', '5c79710f9bb7853e24dc39a4']:
            #     continue

            # 275 sacremento meter exclusion
            # if meter['_id'] in ['5cd982331e8c5d2433594815', '5d517a0dbea676372ee15ea8', '5c796ebb9bb7853e24d9708f', '5c79718b9bb7853e24dc9b2d', '5c79710f9bb7853e24dc39a4', '5cd982331e8c5d2433594815']:
            #     continue
            # if meter['meter_number'] in ['3904-0717']:
            #     continue
            # print(meter)
            # if meter['meter_number'] == 'TNEER103442':
            #     meter['meter_multiplier'] = 0.39

            if use_consumption and consumption_data is not None and \
                    meter['sensor_id'] in consumption_data.columns.values:
                chunked_data = consumption_data[meter['sensor_id']]
                diff_ = [i[0] for i in pd.DataFrame(chunked_data).values
                                        - pd.DataFrame(chunked_data).values[0]]
                chunked_data = [j_ if i_ >= 0 else float('NaN') for i_, j_ in
                     zip(diff_, chunked_data)]
                chunked_data = pd.DataFrame(chunked_data).interpolate(method='linear',
                                                limit_direction='both').sum(axis=1)
                chunked_data = pd.Series(chunked_data.values,
                                            index=consumption_data.index)
                charges, statement_entry, measurement_entries = \
                    generate_report_helper(
                        rules=submeter_rules, mode='consumption',
                        data=chunked_data,
                        start_date=start_date, end_date=end_date,
                        meter_id=meter['_id'],
                        meter_multiplier=meter['meter_multiplier'],
                        calculate_consumption=True,
                        on_peak_start=on_peak_start_time)

                charges_list.append(charges)
                consumption_report.append(statement_entry)
                consumption_measurements.append(measurement_entries)

            elif demand_data is not None and\
                    meter['sensor_id'] in demand_data.columns.values:
                if 'correction_factor' in meter.keys():
                    meter['correction_factor'] = meter['correction_factor']
                else:
                    meter['correction_factor'] = None
                charges, statement_entry, measurement_entries =\
                    generate_report_helper(
                        rules=[grouped_demand_rules, normal_rules],
                        mode='demand', data=demand_data[meter['sensor_id']],
                        start_date=start_date, end_date=end_date,
                        meter_id=meter['_id'],
                        meter_multiplier=meter['meter_multiplier'],
                        meter_correction_factor=meter['correction_factor'],
                        calculate_consumption=False,
                        on_peak_start=None)
                charges_list.append(charges)
                demand_report.append(statement_entry)
                demand_measurements.append(measurement_entries)
        # print('charges_list', charges_list)
        # total_charges = sum(charges_list)
        total_charges = np.nansum(charges_list)

        total_meters = find_distinct_meters(meters_doc=billing_meters)
        cumulative_charges, cumulative_report_cache, cumulative_measurements = \
            process_cumulative_rules(
                cumulative_rules, cumulative_report_cache,
                subtotal_dict=subtotal_dict, total_meters=total_meters,
                start_date=start_date, end_date=end_date)

        total_charges += cumulative_charges
        if consumption_report:
            billing_report += [item for sublist in consumption_report
                                                for item in sublist]
        if demand_report:
            billing_report += [item for sublist in demand_report
                            for item in sublist]
        if cumulative_report_cache:
            billing_report += cumulative_report_cache
        if consumption_measurements:
            measurements_list += [item for sublist in consumption_measurements
                            for item in sublist]
        if demand_measurements:
            measurements_list += [item for sublist in demand_measurements
                              for item in sublist]
        if len(cumulative_measurements) > 0:
            measurements_list.extend(cumulative_measurements)
        statement = prepare_statement(
            account_id=account['_id'], ns=building, company=company,
            new_charges=total_charges, charges_list=billing_report,
            measurements=measurements_list,start_date=start_date,
            end_date=end_date, schedule=schedule, service_type=get_service_type(),
            period_id=period_id, statement_type=statement_type,
            provider=entity_name, meter_number=meter['meter_number'], statement_number_prefix=statement_number_prefix)
        statement = remove_duplicate_measurements(statement)
        statement = update_measurement_type_wording(statement)
        statement = update_group_name(statement)
        billing_reports.append(statement)
        save_report(company=company, building=building, account_number=account['_id'],
                    statements=billing_reports, entity=entity_name)

    return total_charges, billing_reports





