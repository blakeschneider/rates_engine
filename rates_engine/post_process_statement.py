
def remove_duplicate_measurements(statement):
    replace_measurement_uuid_obj = {}
    measurement_uuid_replacement_obj = {}
    updated_measurements = []

    for measurement in statement["measurements"]:
        replace_measurement_uuid_obj[measurement["meter_id"]] = {}

    for measurement in statement["measurements"]:
        replace_measurement_uuid_obj[measurement["meter_id"]][measurement["measurement_type"]] = measurement["measurement_uuid"]

    for meter_id in replace_measurement_uuid_obj:
        if ('electric_demand_onpeak' in replace_measurement_uuid_obj[meter_id]) and ('electric_demand_offpeak' in replace_measurement_uuid_obj[meter_id]):
            measurement_uuid_replacement_obj[replace_measurement_uuid_obj[meter_id]["electric_demand_offpeak"]] = replace_measurement_uuid_obj[meter_id]["electric_demand_onpeak"]

    for charge in statement["charges"]:
        if charge["measurement_uuid"] in measurement_uuid_replacement_obj:
            charge["measurement_uuid"] = measurement_uuid_replacement_obj[charge["measurement_uuid"]]

    for measurement in statement["measurements"]:
        if measurement["measurement_uuid"] not in measurement_uuid_replacement_obj:
            updated_measurements.append(measurement)

    statement["measurements"] = updated_measurements

    return statement

def update_measurement_type_wording(statement):
    wording_replacement = {
        "electric_consumption_general": "consumption",
        "electric_demand_general": "demand",
        "electric_demand_onpeak": "demand",
        "energy_consumption_general": "consumption",
        "energy_demand_general": "demand",
    }

    for measurement in statement["measurements"]:
        if measurement["measurement_type"] in wording_replacement:
            measurement["measurement_type"] = wording_replacement[measurement["measurement_type"]]

    return statement

def update_group_name(statement):
    for charge in statement["charges"]:
        charge["group_name"] = charge["group_name"].lower().replace(' ', '_')

    return statement