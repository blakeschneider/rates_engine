from datetime import timedelta
import pandas as pd
import numpy as np
from scipy import integrate

from .utils import retrieve_limit_pointer, save_limit_pointer


def calculate_charge(total_units, per_unit_charge):
    """
    Takes total units and charge per unit, returns the total charge.

    :param total_units: float, total number of units.
    :param per_unit_charge: float, charge per unit.
    :return: float, total charge calculated
    """
    return total_units * per_unit_charge


def demand_calculation_logic(data, window_length):
    """
    Returns the max demand over given time period based upon
    provided time window length.

    :param data: pd.dataframe, demand data timeseries.
    :param window_length: string, eg:30min
    :return: integer, max_demand calculated
    """
    # data = data.resample('15min').mean().interpolate()
    mean_window_demand = data.rolling('15min').mean()
    return mean_window_demand.max()


def validate_chunk(
        rule, start_datetime, end_datetime, holidays_list):
    start_datetime = pd.to_datetime(start_datetime)

    end_datetime = pd.to_datetime(end_datetime)
    start_day = start_datetime.weekday()
    end_day = end_datetime.weekday()
    # For standardization, we are using both days and months starting from 0
    # say month 1 will become month 0
    start_month = start_datetime.month - 1
    end_month = end_datetime.month - 1

    if not (max(pd.to_datetime(start_datetime).date(),
                pd.to_datetime(rule['start_date']).date())
            <= min(pd.to_datetime(end_datetime).date(),
                   pd.to_datetime(rule['end_date']).date())):
        return False, 0, 0

    if start_month not in rule['months_applicable']\
            and end_month not in rule['months_applicable']:
        return False, 0, 0

    if start_month not in rule['months_applicable']:
        start_datetime = pd.to_datetime(
            start_datetime).date().strftime('%Y-%m-%d') \
                         + ' ' + '23:59'
    if end_month not in rule['months_applicable']:
        end_datetime = pd.to_datetime(
            end_datetime).date().strftime('%Y-%m-%d') + ' ' + '00:00'
    if start_day not in rule['days_applicable']\
            and end_day not in rule['days_applicable']:
        return False, 0, 0

    if start_day not in rule['days_applicable']:
        start_datetime = pd.to_datetime(
            start_datetime).date().strftime('%Y-%m-%d') \
                         + ' ' + '23:59'
    if end_day not in rule['days_applicable']:
        end_datetime = pd.to_datetime(
            end_datetime).date().strftime('%Y-%m-%d') + ' ' + '00:00'

    if rule['holidays_applicable']:
        return True, start_datetime, end_datetime

    if (start_datetime in holidays_list and
            end_day in holidays_list):
        return False, 0, 0

    start_datetime = pd.to_datetime(start_datetime)
    end_datetime = pd.to_datetime(end_datetime)

    if start_datetime.date() in holidays_list:
        start_datetime = pd.to_datetime(start_datetime).date().strftime(
            '%Y-%m-%d') + ' ' + '00:00'
    if end_datetime.date() in holidays_list:
        end_datetime = pd.to_datetime(
                    end_datetime).date().strftime('%Y-%m-%d') + ' ' + '00:00'
    return True, start_datetime, end_datetime


def start_time_greater_than_end(start_time, end_time):
    if int(start_time.split(':')[1]) > int(end_time.split(':')[1]):
        return True


def calculate_consumption(
        chunked_data, chunked_dates, rule, holidays_list, pointer_map, mode):

    total_consumption = 0
    ceil_pointer = False
    # use this counter to determine the start of the chunk,
    # because of floor limit the start_time might not be valid
    # for first chunk.
    chunked_data1 = chunked_data
    chunk_counter = 1
    for date in chunked_dates:
        chunked_data = chunked_data1[chunked_data1.index.strftime(
            '%Y-%m-%d') == str(date)]
        if not date <= pd.to_datetime(rule['end_date']).date():
            continue
        end_datetime = date.strftime('%Y-%m-%d') + ' ' + rule['end_time']
        start_datetime = date.strftime('%Y-%m-%d') + ' ' + rule['start_time']

        valid, start, end = validate_chunk(
            rule=rule, start_datetime=start_datetime,
            end_datetime=end_datetime, holidays_list=holidays_list)
        if chunk_counter == 1 and rule['floor_limit'] != -1:
            pointer = retrieve_limit_pointer(
                    pointer_map=pointer_map, rate_unit=rule['rate_unit'],
                    rule_floor_limit=rule['floor_limit'])
            if pointer is not None:
                start = pointer
        chunk_counter += 1
        if valid:
            if pd.to_datetime(start) > pd.to_datetime(end):
                if (end + timedelta(days=1)).date() in chunked_dates:
                    end = end + timedelta(days=1)
                else:
                    end_date = end.date()
                    end = pd.Timestamp(end_date.strftime('%Y-%m-%d') + ' 23:59')
            time_diff = chunked_data.index[1]-chunked_data.index[0]
            reading_freq = int(str(time_diff).split(':')[1])
            reading_freq = 60 / reading_freq

            start_time = start.time()
            end_time = end.time()

            if mode == 'consumption_from_demand':

                chunk_sum = integrate.trapz(chunked_data.\
                            between_time(start_time, end_time)) / reading_freq
            if mode == 'consumption_from_consumption':
                try:
                    chunk_start = chunked_data[start]
                except Exception as e :
                    chunk_start = chunked_data.iloc[0]

                try:
                    chunk_end = chunked_data[end]
                except KeyError:
                    chunk_end = chunked_data.iloc[-1]
                chunk_sum = chunk_end - chunk_start

            if rule['ceiling_limit'] == -1:
                total_consumption = total_consumption + chunk_sum
                # print('cunk sum ', start, end, chunk_sum, total_consumption)
                continue

            if total_consumption + chunk_sum <= rule['ceiling_limit']:

                total_consumption = total_consumption + chunk_sum
            else:
                result, ceil_pointer = process_ceil_exceeding_chunk(
                    total_consumption=total_consumption,
                    chunk=chunked_data.between_time(start_time, end_time),
                    ceil_limit=rule['ceiling_limit'],
                    start_datetime=start, mode=mode, reading_freq=reading_freq
                )
                total_consumption += result
                break

    if rule['ceiling_limit'] > 0 and ceil_pointer:
        pointer_location = ceil_pointer
        save_limit_pointer(
            pointer_map=pointer_map, measurement_value=rule['ceiling_limit']+1,
            calculated_on=rule['rate_unit'], data_index=pointer_location)
    return total_consumption


def process_ceil_exceeding_chunk(
        total_consumption, chunk, ceil_limit, start_datetime, mode,
        reading_freq):
    index_pointer = 0
    previous_sum = 0
    start_datetime = pd.to_datetime(start_datetime)
    for index, value in chunk.iteritems():
        if mode == 'consumption_from_demand':
            current_sum = integrate.trapz(
                     chunk.ix[start_datetime:index])/reading_freq
        if mode == 'consumption_from_consumption':
            current_sum = chunk.ix[index] - chunk.ix[start_datetime]
        if total_consumption + current_sum > ceil_limit:
            break
        else:
            previous_sum = current_sum
            index_pointer = index
    return previous_sum, index_pointer


def chunk_data_with_rule(data, rule, pointer_map):
    """
    Chunk the data according to rules and return the measurement total and
     list of chunks.

    This function takes the data and rule as an argument and provide the list
    of valid data points which follow the rule. Also, it calculates the
    cumulative measurement using the data points.(eg: consumption or demand.)

    :param data: pd.timeseries, time series of data to be chunked
    :param rule: python dictionary, timestamp, rule applied to chunk the time
                series
    :param pointer_map: python dictionary, map to store ceil and floor pointers
    :return: list, list; the list of chunks, list of dates.
    """
    if not rule['floor_limit'] == -1:
        start_pointer = retrieve_limit_pointer(
            pointer_map=pointer_map, rate_unit=rule['rate_unit'],
            rule_floor_limit=rule['floor_limit'])
        data = data[start_pointer:]
    chunked_data = data.between_time(rule['start_time'], rule['end_time'])
    chunked_dates = np.unique(chunked_data.index.date)
    return chunked_data, chunked_dates
