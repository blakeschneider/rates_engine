
from dateutil import relativedelta
from datetime import timedelta

import pandas as pd

from .utils import retrieve_subtotal, save_measurement,\
                    retrieve_measurement, save_subtotal,\
                    prepare_measurement_entry, prepare_charge_entry,\
                    include_measurement
from .rates_logic import calculate_charge, chunk_data_with_rule,\
                        demand_calculation_logic, validate_chunk,\
                        calculate_consumption
from .statement_builder import unique_measurement_id, include_zero_meters

def classify_rules(rules):
    """
    Separate the rules into submeter and cumulative classes.

    The submeter rules are applicable to every submeter. The cumulative rules
    are applied to the cumulative charges of all the submeters.

    :param rules: pd.DataFrame, dataframe of rules.
    :return: pd.DataFrame, pd.DataFrame,
            DataFrames of submeter rules, cumulative rules.
    """
    submeter_rules = rules.loc[
        ~rules['calculated_on'].isin(['constant', 'total'])]
    cumulative_rules = rules.loc[
        rules['calculated_on'].isin(['constant', 'total'])]
    return submeter_rules, cumulative_rules


def filter_rules(rules, start_date, end_date):
    """
    Loads the rules for given provider which falls in given date time range.

    :param rules: pd.DataFrame, DataFrame of rules.
    :param start_date: string, start_date of the rule
    :param end_date: string, end date of rule
    :return: pd.DataFrame, DataFrame of filtered rules
    """
    # print('rules getting into filter_rules', rules)
    # print('start_date getting into filter_rules', start_date)
    # print('end_date getting into filter_rules', end_date)
    rules.loc[(rules.end_date == 'None'), 'end_date'] = pd.to_datetime(
        'today').strftime('%Y-%m-%d')
    df = pd.DataFrame(
        [rule for _, rule in rules.iterrows()
         if (
            max(pd.to_datetime(start_date), pd.to_datetime(rule['start_date']))
            <= min(
             pd.to_datetime(end_date), pd.to_datetime(rule['end_date'])))]
            )
    return df


def build_rule_entries(rules):
    for index, rule in rules.iterrows():
        if len(rule['parent_rules']) < 1:
            rules.loc[index, 'charge_entry'] = rule['charge_type']
        else:

            for i in rule['parent_rules']:
                rules.loc[index, 'charge_entry' +'_'+str(i)] = ''
                if len(rules.loc[index, 'charge_entry'+'_'+str(i)]) < 1:
                    rules.loc[index, 'charge_entry' +'_'+str(i)] += rules[rules._id == i].iloc[0]['charge_type']
                else:
                    rules.loc[index, 'charge_entry'+'_'+str(i)] += ' > ' + \
                    rules[rules._id == i].iloc[0]['charge_entry']
                rules.loc[index, 'charge_entry'+'_'+str(i)] +=' > '+ rules.loc[index, 'charge_type']
    return rules


def filter_demand_rules_with_ceil_and_floor(rules):
    """
    Filters demand rules with ceil and floor limits from provided dataframe of
    rules.

    :param rules: pd.DataFrame, DataFrame of rules.
    :return: pd.DataFrame, pd.DataFrame
            DataFrames of demand rules and other rules respectively.
    """
    filtered_rules = rules[~(rules.parent_rules.str.len() > 0)]
    filtered_rules = filtered_rules[~(filtered_rules.floor_limit == -1) &
                                    ~(filtered_rules.ceiling_limit == -1)]
    demand_rules = filtered_rules[filtered_rules.rate_unit == 'kw']
    other_rules = rules[~(rules._id.isin(demand_rules._id))]
    return demand_rules, other_rules


def group_and_sort_demand_rules(demand_rules):
    """
    Group the provided rules based upon their start time, end time,
    days applicable. Then sort the rules according to floor_limit within their
    respective groups.

    :param demand_rules:
    :return:
    """
    grouping_fields = ['start_time', 'end_time', 'days_applicable']
    demand_rule_groups = demand_rules.groupby(grouping_fields, sort=False)
    demand_groups = []
    for key in demand_rule_groups.groups.keys():
        rule_group_temp = demand_rules.loc[
            (demand_rules['start_time'] == key[0])
            & (demand_rules['end_time'] == key[1])
            & (demand_rules['days_applicable'] == key[2])
            ]
        rule_group = rule_group_temp
        demand_groups.append(rule_group)
    sorted_groups = []
    for group in demand_groups:
        sorted_groups.append(group.sort_values('floor_limit'))

    return sorted_groups


def process_demand_groups(demand_groups, demand_data, pointer_map,
                          subtotal_dict, measurement_dict, holidays_list,
                          report_cache, start_date, end_date, meter_id,
                          meter_multiplier):
    """
    Applies demand group rules to the data. Only one rules among the group will
    be applicable to data based upon whose floor and ceil values fits the
    calculated demand.

    Assumption:  There will never be overlapping floor and ceiling limits for
                  two rules. eg if a rule start at 0 and ends at 20 next rule
                  should start at 21 and end at 22+.

    :param demand_groups: list, list of rule dataframe groups.
    :param demand_data: data on which the rule is applied.
    :param pointer_map:  dictionary, dictionary to store the mapping of
                        ceiling limit to data index.
    :param subtotal_dict: dictionary, dictionary to store the calculated
                        charges to be used by child rules(format
                        ruleid: charge).
    :param measurement_dict: dictionary, dictionary to store the calculated
                            consumption.
    :param holidays_list: holidays object, contains the list of holidays.
    :param report_cache: list, list to store report entries.
    :param start_date: string, start date of billing interval.
    :param end_date: string, end date of billing interval.
    :param meter_id: string, id of the current meter.
    :param meter_multiplier: float, meter multiplier for the meter.
    :return: float, dictionary
     Total charges calculated using the rule, measurement description
     in key value form.
    """
    total_charges = 0
    measurement_entries = list()
    for group in demand_groups:

        rule_charges = 0
        rule = group.iloc[0]
        chunked_data, chunked_dates = \
            chunk_data_with_rule(
                demand_data, rule, pointer_map=pointer_map)
        if len(chunked_data) > 0:
            valid_chunks = list()
            for date in chunked_dates:
                start_datetime = \
                    date.strftime('%Y-%m-%d') + ' ' + rule['start_time']
                end_datetime = \
                    date.strftime('%Y-%m-%d') + ' ' + rule['end_time']
                valid, start, end = validate_chunk(
                    rule, start_datetime, end_datetime, holidays_list)
                if valid:
                    valid_chunks.append(chunked_data[start:end])
        max_demand = max([demand_calculation_logic(chunk, rule['time_window'])
                          for chunk in valid_chunks])
        corrected_demand = meter_multiplier * max_demand
        for _, rule in group.iterrows():
            if rule['floor_limit'] <= corrected_demand \
                    <= rule['ceiling_limit']:
                rule_charges = calculate_charge(corrected_demand,
                                                rule['value'])
                break
        save_subtotal(subtotal_dict, rule['_id'], rule_charges)
        save_measurement(measurement_dict, rule['rate_unit'].lower(),
                         rule['_id'], corrected_demand)
        if rule_charges > 0:

            prepare_charge_entry(
                rule=rule, charge=rule_charges, start_date=start_date,
                end_date=end_date, report_cache=report_cache,
                meter_id=meter_id, total_measurement=corrected_demand)

            entry, _ = prepare_measurement_entry(
                meter_id=meter_id, measurement_unit=rule['rate_unit'],
                meter_reading_delta=max_demand,
                corrected_reading=corrected_demand,
                measurement_type='demand',
                data=demand_data,
                rule=rule,
                start_date=start_date,
                end_date=end_date,
                pointer_map=pointer_map,
                meter_multiplier=meter_multiplier
            )
            if len(entry) > 0:
                measurement_entries.append(entry)
            total_charges += rule_charges
    return total_charges, measurement_entries


def calculate_demand_from_consumption(data, window_size):
    """
    Calculate demand from consumption data.
    Uses the rolling window and returns the max consumption in given window
    time calculated over the billing period.

    :param data: pd.dataframe,
    :param window_size: size of time window eg: 10min, 20min
    :return: float, the max demand over given time series.
    """
    data_freq = abs(data.index[1].minute - data.index[0].minute)
    pointer_difference = window_size / data_freq
    return data.diff(pointer_difference).max()


def process_measurement_children(
        rules, measurement_dict, report_cache, start_date, end_date,
        meter_id, group_type_cache, subtotal_dict, measurement_uuid_cache):
    """
    Process the rules dependent upon the measurement calculated by other rules.

    :param rules: list,
        List of rules dependent on the measurement of the other rules.
    :param measurement_dict: dictionary;
        Dictionary containing the mapping of parent rules to the measurements.
    :param report_cache: list; List to store intermediate results.
    :param start_date: string, billing statement start date.
    :param end_date: string, billing statement end date.
    :param meter_id: string, meter identifier or meter id.
    :return: float; Total charge calculated over the rules.
    """
    total_charges = 0
    for rule in rules:
        parent_measurement_list = retrieve_measurement(measurement_dict,
                                                       rule['rate_unit'])
        total_measurement = 0
        if parent_measurement_list is None:
            continue
        for parent_measurement in parent_measurement_list:
            if parent_measurement['rule_id'] in rule['parent_rules']:
                parent_rule_measurement = parent_measurement['total']
                total_measurement += parent_rule_measurement
                rule_charge = calculate_charge(total_units=parent_rule_measurement,
                                       per_unit_charge=rule['value'])
                prepare_charge_entry(
                    rule=rule, charge=rule_charge, start_date=start_date,
                    end_date=end_date, report_cache=report_cache,
                    meter_id=meter_id, total_measurement=parent_rule_measurement,
                    group_type_cache=group_type_cache,
                    parent_id=parent_measurement['rule_id'],
                    measurement_uuid_cache=measurement_uuid_cache)

        total_charges += rule_charge
        save_subtotal(subtotal_dict, rule['_id'], total_charges)
        save_measurement(measurement_dict, rule['rate_unit'].lower(),
                         rule['_id'], total_measurement)

    return total_charges


def process_subtotal_child_rules(
        subtotal_rules, subtotal_dict, report_cache, start_date, end_date,
        meter_id, group_type_cache, measurement_uuid_cache):
    """
    Process rules dependent on the subtotal of the other rules.

    :param subtotal_rules: list, list of subtotal dependent rules.
    :param subtotal_dict: python dictionary,
            Dictionary containing the mapping of parent rules to the charges.
    :param start_date: datetime, Billing statement start date.
    :param end_date: datetime, Billing statement end date.
    :param report_cache: list, list to store report entries.
    :param meter_id: string, meter identifier or meter id.
    :return: float, aggregate charge calculated over the list of rules.
    """
    total_rules_charge = 0
    for rule in subtotal_rules:
        subtotal_rule_total = 0
        for parent_id in rule['parent_rules']:
            parent_subtotal = retrieve_subtotal(subtotal_dict=subtotal_dict,
                                                rule_id=parent_id)

            parent_rules_total = parent_subtotal
            if rule['rate_unit'] == '%':
                processed_charges = calculate_charge(
                    total_units=parent_subtotal, per_unit_charge=rule['value'] / 100)
            else:
                processed_charges = calculate_charge(
                    total_units=parent_subtotal, per_unit_charge=rule['value'])
            subtotal_rule_total += processed_charges
            prepare_charge_entry(
                rule=rule, charge=processed_charges,
                start_date=start_date, end_date=end_date,
                report_cache=report_cache, meter_id=meter_id,
                total_measurement=parent_rules_total,
                group_type_cache=group_type_cache,
                measurement_uuid_cache=measurement_uuid_cache,
                parent_id=parent_id,
            )
        save_subtotal(subtotal_dict, rule['_id'], subtotal_rule_total)
        total_rules_charge += subtotal_rule_total

    return total_rules_charge


def process_constant_rules(
        subtotal_dict, const_rules, report_cache, start_date, end_date,
        meter_id):
    """
    Calculate measurement independent charges to the bill. These charges can be
    monthly or  per bill.

    :param subtotal_dict: python dictionary
        Dictionary to store the resultant calculation.
    :param const_rules: list
        List of the rules with index [0] representing the number of units and
    index [1] represents the charge amount. for eg: [[2,.001]] where 2 might
    be 2 months and .001 charges per month.
    :param report_cache: list, list to temporarily store statement entries.
    :param start_date: string, billing statement start date.
    :param end_date: string, billing statement end date.
    :param meter_id: string, meter identifier.
    :return: float, total charge calculated over the rules.
    """
    constant_total = 0
    constant_measurements = []
    for num_units, rule in const_rules:
        charge = calculate_charge(total_units=num_units,
                                  per_unit_charge=rule['value'])
        constant_total += charge
        save_subtotal(subtotal_dict=subtotal_dict, rule_id=rule['_id'],
                      total=charge)
        measurement_uuid = unique_measurement_id()
        measurement, _ = prepare_measurement_entry(
            meter_id=None, measurement_unit=rule['rate_unit'],
            meter_reading_delta=num_units, corrected_reading=num_units,
            start_date=start_date, end_date=end_date, rule=rule,
            measurement_type='global_variable', data=None,
            calculation_type=None, pointer_map=None,
            measurement_uuid=measurement_uuid, meter_multiplier=None)
        constant_measurements.append(measurement)
        prepare_charge_entry(
            rule=rule, charge=charge, start_date=start_date, end_date=end_date,
            report_cache=report_cache, meter_id=None, total_measurement=num_units,
            measurement_uuid=measurement_uuid, measurement_uuid_cache={})

    return constant_total, constant_measurements


def process_consumption_rules(
        rule, consumption_data, subtotal_dict, measurement_dict, pointer_map,
        holidays_list, report_cache, start_date, end_date, meter_id,
        meter_multiplier, calculation_type, measurement_uuid_cache,
        measurement_entries_list):
    """
    Apply consumption dependent rules to the data.
    uses the data chunking logic and returns the total charges with entries.

    :param rule: dictionary, rule to be applied on the data.
    :param consumption_data: pd.dataframe, data on which the consumption rules
                            are applied.
    :param subtotal_dict: dictionary,
        Dictionary to store the calculated charges to be used by child rules(
    format ruleid:charge).
    :param measurement_dict: dictionary;
        Dictionary to store the calculated consumption
    :param pointer_map: dictionary;
        Dictionary to store the mapping of ceiling limit to data index. For
    example, if a rule has ceil limit of 1000. The rule will store 1001 as
    mapping to the current_index+1 as the next rule needs to know from where it
    should start its calculation.)
    :param holidays_list: holidays object, holidays for current country.
    :param report_cache: list, list to temporarily store report entries.
    :param start_date: string, start date of the billing period.
    :param end_date: string, end date of the billing period.
    :param meter_id: string,  id of the current meter in consideration.
    :param meter_multiplier: float, meter multiplier for current meter.
    :param calculation_type: string
        what type of data we are using? consumption_from_consumption or
    Consumption_from_demand. Can take one of these values based upon data.
    :return: float, total charges calculated using the rule.
    """
    total_charges = 0
    existing_uuid = None
    measurement_entry = {}
    chunked_data, chunked_dates = chunk_data_with_rule(
        data=consumption_data, rule=rule, pointer_map=pointer_map)
    rule_consumption = calculate_consumption(
        chunked_data=chunked_data, chunked_dates=chunked_dates, rule=rule,
        holidays_list=holidays_list, pointer_map=pointer_map,
        mode=calculation_type)
    corrected_consumption = rule_consumption * meter_multiplier
    temp_charges = 0
    temp_rule_charges = calculate_charge(total_units=corrected_consumption,
                                    per_unit_charge=rule['value'])
    temp_charges += temp_rule_charges
    if include_zero_meters() or temp_charges > 0:
        measurement_uuid = unique_measurement_id()

        measurement_entry, corrected_consumption = prepare_measurement_entry(
            meter_id=meter_id, measurement_unit=rule['rate_unit'],
            meter_reading_delta=rule_consumption,
            corrected_reading=corrected_consumption,
            measurement_type='consumption',
            measurement_uuid=measurement_uuid,
            data=consumption_data,
            start_date=start_date,
            end_date=end_date,
            rule=rule, calculation_type=calculation_type,
            pointer_map=pointer_map,
            meter_multiplier=meter_multiplier
            )

        # corrected_consumption = rule_consumption * meter_multiplier
        rule_charges = calculate_charge(total_units=corrected_consumption,
                                        per_unit_charge=rule['value'])
        total_charges += rule_charges

        save_subtotal(subtotal_dict=subtotal_dict, rule_id=rule['_id'],
                      total=rule_charges)
        save_measurement(measurement_dict=measurement_dict,
                         measurement_type=rule['rate_unit'].lower(),
                         rule_id=rule['_id'], total=corrected_consumption)
        include_entry, existing_uuid = include_measurement(
                    measurements_list=measurement_entries_list,
                    test_measurement=measurement_entry)
        if include_entry:
            measurement_uuid_cache[rule['_id']] = measurement_uuid
        else:
            measurement_uuid = existing_uuid
            measurement_uuid_cache[rule['_id']] = measurement_uuid
        prepare_charge_entry(rule=rule, charge=rule_charges, start_date=start_date,
                             end_date=end_date, report_cache=report_cache,
                             meter_id=meter_id,
                             total_measurement=corrected_consumption,
                             measurement_uuid=measurement_uuid,
                             measurement_uuid_cache=measurement_uuid_cache)
    if existing_uuid is not None:
        return total_charges, None
    return total_charges, measurement_entry


def process_demand_rules(rule, demand_data, demand_definition, pointer_map,
                         subtotal_dict, measurement_dict, holidays_list,
                         report_cache, start_date, end_date,
                         meter_id, meter_multiplier, measurement_uuid_cache,
                         measurement_entries_list,
                         meter_correction_factor=None):
    """
    Apply demand rules to the data. These rules are only applicable if demand
    data is available.

    :param rule: pd.dataframe.row, rule to be applied.
    :param demand_data: data on which the rule is applied.
    :param demand_definition: pd.dataframe, definition of the demand for
                            given provider.
    :param pointer_map:  dictionary, dictionary to store the mapping of
                        ceiling limit to data index.
    :param subtotal_dict: dictionary, dictionary to store the calculated
                        charges to be used by child rules(format
                        ruleid: charge).
    :param measurement_dict: dictionary, dictionary to store the calculated
                            consumption.
    :param holidays_list: holidays object, contains the list of holidays.
    :param report_cache: list, list to store report entries.
    :param start_date: string, start date of billing interval.
    :param end_date: string, end date of billing interval.
    :param meter_id: string, id of the current meter.
    :param meter_multiplier: float, meter multiplier for the meter.
    :return: float, dictionary
     Total charges calculated using the rule, measurement description
     in key value form.
    """
    existing_uuid = None
    total_charges = 0
    measurement_entry = list()
    chunked_data, chunked_dates = \
        chunk_data_with_rule(demand_data, rule, pointer_map=pointer_map)
    max_demand = 0
    if len(chunked_data) > 0:
        valid_chunks = list()
        for date in chunked_dates:
            start_datetime =\
                date.strftime('%Y-%m-%d') + ' ' + rule['start_time']
            if pd.to_datetime(rule['start_time']) > pd.to_datetime(rule['end_time']):
                date = pd.to_datetime(date.strftime('%Y-%m-%d')) + timedelta(days=1)
            end_datetime =\
                date.strftime('%Y-%m-%d') + ' ' + rule['end_time']
            valid, start, end = \
                validate_chunk(rule, start_datetime, end_datetime,
                                holidays_list)
            if valid:
                valid_chunks.append(chunked_data[start:end])
        max_demand = max([demand_calculation_logic(chunk, demand_definition)
                          for chunk in valid_chunks])
    corrected_demand = meter_multiplier * max_demand
    if meter_correction_factor is not None:
        corrected_demand = corrected_demand * meter_correction_factor
    rule_charges = calculate_charge(corrected_demand, rule['value'])
    if rule['demand_fit']:
        days_delta = (pd.to_datetime(end_date) - pd.to_datetime(start_date)).days
        rule_charges = rule_charges * (days_delta/30)
    total_charges += rule_charges
    save_subtotal(subtotal_dict, rule['_id'], rule_charges)
    save_measurement(measurement_dict, rule['rate_unit'].lower(),
                     rule['_id'], corrected_demand)
    # print('corrected_demand', corrected_demand)
    if include_zero_meters() or total_charges > 0:
        measurement_uuid = unique_measurement_id()
        measurement_entry, _ = prepare_measurement_entry(
            meter_id=meter_id, measurement_unit=rule['rate_unit'],
            meter_reading_delta=max_demand,
            corrected_reading=corrected_demand,
            measurement_type='demand',
            data=demand_data,
            rule=rule,
            start_date=start_date,
            end_date=end_date,
            pointer_map=pointer_map,
            measurement_uuid=measurement_uuid,
            meter_multiplier=None
        )
        include_entry, existing_uuid = include_measurement(
            measurements_list=measurement_entries_list,
            test_measurement=measurement_entry)
        if include_entry:
            measurement_uuid_cache[rule['_id']] = measurement_uuid
        else:
            measurement_uuid = existing_uuid
            measurement_uuid_cache[rule['_id']] = measurement_uuid

        prepare_charge_entry(
            rule=rule, charge=rule_charges, start_date=start_date,
            end_date=end_date, report_cache=report_cache, meter_id=meter_id,
            total_measurement=corrected_demand, measurement_uuid=measurement_uuid,
            measurement_uuid_cache=measurement_uuid_cache)

    if existing_uuid is not None:
        return total_charges, None
    return total_charges, measurement_entry


def process_monthly_rules(rules, subtotal_dict, start_date, end_date,
                          report_cache, meter_id):
    """
    Apply charges calculated per month to the bill.

    :param rules: list, list of monthly applicable rules.
    :param subtotal_dict: dictionary
        Python dictionary to store the intermediate for future usage.
    :param start_date: date, billing statement start date.
    :param end_date: date, billing statement end date.
    :param report_cache: list, temporarily stores report entries.
    :param meter_id: string, id of the billing meter.
    :return: float, total aggregated charges calculated.
    """
    months = relativedelta.relativedelta(pd.to_datetime(end_date),
                                         pd.to_datetime(start_date)).months
    # Monthly charges will be applicable atleast for one month, irrespective
    # days.
    months = 1 if months == 0 else months

    total_charges = 0
    for rule in rules:
        charge = months * rule['value']
        total_charges += charge
        save_subtotal(subtotal_dict=subtotal_dict, rule_id=rule['_id'],
                      total=charge)

        prepare_charge_entry(
            rule=rule, charge=total_charges, start_date=start_date,
            end_date=end_date, report_cache=report_cache, meter_id=meter_id,
            total_measurement=months)
    return total_charges


def process_total_child_rules(total_dependent_rules, total_charges,
                              report_cache, start_date, end_date, meter_id):
    """
    Apply charges on total bill amount. For eg: tax or surcharge, which are
    calculated on total amount of the bill.

    :param total_dependent_rules: list, list of total dependent rules.
    :param total_charges: float, total calculated bill.
    :param report_cache: list, cache to store the results.
    :param start_date: string, billing statement start date.
    :param end_date: string, billing statement end date.
    :param meter_id: string,  billing meter identifier.
    :return: float, charges calculated on the bill.
    """
    rules_total = 0
    for rule in total_dependent_rules:
        rules_total += calculate_charge(total_units=total_charges,
                                        per_unit_charge=rule['value'])
        prepare_charge_entry(
            rule=rule, charge=rules_total, start_date=start_date,
            end_date=end_date, report_cache=report_cache, meter_id=meter_id,
            total_measurement=total_charges)
    return rules_total


def process_cumulative_rules(cumulative_rules, cumulative_report_cache,
                             subtotal_dict, total_meters, start_date,
                             end_date):
    """
    Process cumulative rules, which are calculated on the total bill
    independent of the submeters.

    :param cumulative_rules: pd.DataFrame, DataFrame of the rules.
    :param cumulative_report_cache: list, stores the report entries
    :param subtotal_dict: python dictionary,
        Store intermediate calculated results
    :param total_meters: int, total number od meters associated with account
    :param start_date: string, start date of billing statement.
    :param end_date:, end date of billing statement.
    :return: float, list; total charges and report entries respectively
    """
    total_charges = 0
    constant_charges = []
    per_month_charges = []
    total_dependent_rules = []
    cumulative_measurements = []
    for _, rule in cumulative_rules.iterrows():
        if rule['calculated_on'].lower() == 'constant':
            if '/' in rule['rate_unit']:
                if rule['rate_unit'].split('/')[1].lower() == 'periods':
                    constant_charges.append([1, rule])

                if rule['rate_unit'].split('/')[1].lower() == 'meters':
                    constant_charges.append([total_meters, rule])
                if rule['rate_unit'].split('/')[1].lower() == 'months':
                    per_month_charges.append(rule)
            else:
                if rule['rate_unit'].lower() == 'periods':
                    constant_charges.append([1, rule])

                if rule['rate_unit'].lower() == 'meters':
                    constant_charges.append([total_meters, rule])
                if rule['rate_unit'].lower() == 'months':
                    per_month_charges.append(rule)

        if rule['calculated_on'].lower() == 'total':
            total_dependent_rules.append(rule)

    if per_month_charges:
        total_charges += process_monthly_rules(
            rules=per_month_charges, subtotal_dict=subtotal_dict,
            start_date=start_date, end_date=end_date,
            report_cache=cumulative_report_cache,
            meter_id=None)

    if constant_charges:
        constant_charges, constant_entries = process_constant_rules(
            subtotal_dict=subtotal_dict, const_rules=constant_charges,
            report_cache=cumulative_report_cache, start_date=start_date,
            end_date=end_date, meter_id=None)
        total_charges += constant_charges
        cumulative_measurements.extend(constant_entries)

    if total_dependent_rules:
        total_charges += process_total_child_rules(
            total_dependent_rules=total_dependent_rules,
            total_charges=total_charges, report_cache=cumulative_report_cache,
            start_date=start_date, end_date=end_date, meter_id=None)
    return total_charges, cumulative_report_cache, cumulative_measurements


def preprocess_ar_rules(rules, account_info):
    for index, rule in rules.iterrows():
        rule['c_or_d'] = str(rule['c_or_d'])
        if rule['c_or_d'].lower() == 'c' and str(rule['tax_type']).lower() == 'commodity_tax':
            rules.set_value(index, 'value',account_info['commodity_tax'] / 100)
        elif rule['c_or_d'].lower() == 'd' and str(rule['tax_type']).lower()=='delivery_tax':
            rules.set_value(index, 'value', account_info['delivery_tax'] / 100)
        if str(rule['tax_type']).lower() == 'sales_tax':
            rules.set_value(index, 'value', account_info['sales_tax'] / 100)
        if str(rule['tax_type']).lower() == 'lease_modification_tax':
            rules.set_value(index, 'value', account_info['lease_modification'] / 100)
    return rules
