import os
import json

from scipy.integrate import trapz
import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar as calendar

from .statement_builder import prepare_statement, get_service_type, get_consumption_service_type, get_demand_service_type


def get_holiday_list(start_date, end_date):
    """
    Returns holidays list for given country.
    Currently supports only US holidays.

    :param start_date: string, start date of statement
    :param end_date: string, end date of the statement.
    :return: holidays object
    """
    cal = calendar()
    holidays = cal.holidays(start=start_date, end=end_date)
    return holidays


def save_subtotal(subtotal_dict, rule_id, total):
    """
    Add the subtotal to the dictionary.

    :param subtotal_dict: dictionary, subtotal dictionary for saving the rule
                        total
    :param rule_id: string, id of the rule.
    :param total: float, charge calculated by the rule.
    :return: None
    """
    if rule_id not in subtotal_dict:
        subtotal_dict[rule_id] = total


def retrieve_subtotal(subtotal_dict, rule_id):
    """
    Return subtotal for requested rule_id if exists otherwise
    return 0

    :param subtotal_dict: dictionary, subtotal dictionary containing the
                        subtotal entries.
    :param rule_id: string, parent rule id to lookup into the dictionary
    :return: float
    """
    if rule_id in subtotal_dict:
        return subtotal_dict[rule_id]
    else:
        return 0


def save_measurement(measurement_dict, measurement_type, rule_id, total):
    """
    Save rule measurements in measurement dictionary rules to
    use in future.

    :param measurement_dict: dictionary, measurement dictionary for saving the
                            rule measurement.
    :param measurement_type: string, eg: kw, kwh.
    :param rule_id: string, id of the rule.
    :param total: float,  total measurement calculated by the rule.
    :return: None.
    """
    item = {'rule_id': rule_id, 'total': total}
    if measurement_type not in measurement_dict:
        measurement_dict[measurement_type] = [item]
    else:
        measurement_dict[measurement_type].append(item)


def retrieve_measurement(measurement_dict, measurement_type):
    """
    Return subtotal for requested rule_id if exists otherwise
    return None

    :param measurement_dict: dictionary, measurement dictionary for saving the
                            rule measurement.
    :param measurement_type:
    :return: list, list of maps including measurement in rule_id: measurement
            format
    """
    if measurement_type in measurement_dict:
        return measurement_dict[measurement_type]


def save_limit_pointer(pointer_map, measurement_value, calculated_on,
                       data_index):
    """
    save the index of ceiling limit pointer of the current rule to be used
    by the next rule applicable whose floor limit is ceil+1 for the current
    rule. The rate_unit differentiate between consumption and demand pointers.

    :param pointer_map: dictionary, map to store the pointer.
    :param measurement_value: integer, current measurement calculated by rule.
    :param calculated_on: string, eg: kw, kwh
    :param data_index: integer, current+1 index of timeseries
    :return: None
    """
    pointer_map[calculated_on] = {measurement_value: data_index}


def retrieve_limit_pointer(pointer_map, rate_unit, rule_floor_limit):
    """
    Returns the index of the data point from where the rule should
    start verification and chunking.

    One use case is ceil and floor limit of the rules. As, the ceil limit
    reaches the next applicable rule should start from the index of(ceil)+1
    index rather than from the begining.

    :param pointer_map: dictionary, map to retrieve the pointer.
    :param rate_unit: string eg: kw, kwh, type of data for which the pointer
                                will be used.
    :param rule_floor_limit: integer, the limit to look into the map.
    :return: integer if pointer found else None.
    """
    if rate_unit in pointer_map:
        if rule_floor_limit in pointer_map[rate_unit]:
            return pointer_map[rate_unit][rule_floor_limit]
    return None


def prepare_measurement_description(description, measurement_type):
    """
    Returns description of the peak type charges. eg: on peak, off peak.

    :param description: string, rule description
    :param measurement_type: string
        Type of the measurement eg: demand, consumption
    :return: string, eg: on_peak_consumption, off_peak_demand
    """
    rule_description = description
    if 'peak' in description.lower():
        if 'on' in description.lower():
            rule_description = 'on peak ' + measurement_type
        if 'off' in description.lower():
            rule_description = 'off peak ' + measurement_type
        return rule_description
    else:
        return description


# def correct_measurement_entry(delta, measurent_delta):
#     if abs(delta - measurent_delta) / measurent_delta < 2:
#         return measurent_delta
#     else:
#         return delta

def correct_measurement_entry(delta, measurent_delta):
    return measurent_delta

def safe_rounding(amount, decimal = 0):
    if pd.isna(amount):
        return float(0)
    else:
        return round(float(amount), decimal)


def prepare_measurement_entry(
        meter_id, measurement_unit, meter_reading_delta, corrected_reading,
        start_date, end_date, rule,meter_multiplier,
        measurement_type, data, calculation_type=None, pointer_map=None,
        measurement_uuid=None, ):
    """
    Prepares key value pair for the measurement entry in the statement.

    :param meter_id: string, id of the billing meter
    :param measurement_unit: string, units of measurement.
    :param meter_reading_delta: float, calculated consumption or demand
    :param corrected_reading: float, corrected value of consumption or demand
    :param start_date: date, statrt date of billing statement.
    :param end_date: date, end date of billing statement.
    :param measurement_type: string, type of the measurement eg: consumption.
    :param data: pd.dataframe, dataframe of data.
    :param rule, pd.dataframe, dataframe of rules.
    :param calculation_type, string
        Type of calculation eg: consumption from demand.
    :param pointer_map, python dictionary, to retrieve limit pointers
    :return: dictionary, python dictionary with measurement key value pairs.
    """

    charge_type = rule['charge_type'].lower()
    if len(rule['rate_unit'].split('/')) > 1 and \
        rule['rate_unit'].split('/')[1].lower() == 'kw':
        if 'on' in charge_type and 'peak' in charge_type:
            measurement_type = get_demand_service_type() + '_onpeak'

        elif 'primary' in charge_type:
            measurement_type = get_demand_service_type() + 'primary'

        elif 'secondary' in charge_type:
            measurement_type = get_demand_service_type() + '_secondary'

        elif 'off' in charge_type and 'peak' in charge_type:
            measurement_type = get_demand_service_type() + '_offpeak'
        else:
            measurement_type = get_demand_service_type() + '_general'

    elif len(rule['rate_unit'].split('/')) > 1 and \
        rule['rate_unit'].split('/')[1].lower() == 'kwh':
        if 'on' in charge_type and 'peak' in charge_type:
            measurement_type = get_consumption_service_type() + '_onpeak'

        elif 'off' in charge_type and 'peak' in charge_type:
            measurement_type = get_consumption_service_type() + '_offpeak'
        else:
            measurement_type = get_consumption_service_type() + '_general'
    else:
        if measurement_type is None:
            if 'admin' in rule['description'].lower():
                measurement_type = 'admin fee'
            elif 'service' in rule['description'].lower():
                measurement_type = 'service fee'
            else:
                measurement_type = 'other'
    interval_start = \
        pd.to_datetime(start_date) if\
            pd.to_datetime(start_date) >= pd.to_datetime(rule['start_date'])\
        else pd.to_datetime(rule['start_date'])
    interval_end = pd.to_datetime(end_date) if \
                        pd.to_datetime(end_date) \
                        <= pd.to_datetime(rule['end_date'])\
                        else pd.to_datetime(rule['end_date'])
    if 'consumption' in measurement_type.split('_'):
        if rule['floor_limit'] != -1:
            interval_start = retrieve_limit_pointer(
                pointer_map=pointer_map, rate_unit='kWh',
                rule_floor_limit=rule['floor_limit'])

            if rule['ceiling_limit'] != -1:
                interval_end = retrieve_limit_pointer(
                    pointer_map=pointer_map, rate_unit='kWh',
                    rule_floor_limit=rule['ceiling_limit'])
            else:
                interval_end = \
                    pd.to_datetime(interval_end+' ' + rule['end_time'])
        else:
            interval_start = \
                pd.to_datetime(str(interval_start)+' ' + rule['start_time'])
            interval_end = \
                pd.to_datetime(str(interval_end) + ' ' + rule['end_time'])

    else:
        meter_previous_reading = 0
        meter_current_reading = corrected_reading
    if '/' in rule['rate_unit'] and rule['rate_unit'].split('/')[1].lower()\
        not in ['kw', 'kwh']:
        meter_previous_reading = 0
        meter_current_reading = meter_reading_delta

    if len(rule['rate_unit'].split('/')) > 1:
        measurement_unit = rule['rate_unit'].split('/')[1]
    if measurement_unit.lower() == 'kwh' and \
            pd.to_datetime(rule['end_time']).strftime('%H:%M') == '23:59' and \
            pd.to_datetime(rule['start_time']).strftime('%H:%M') == '00:00':
        meter_previous_reading = data.iloc[0]
        meter_current_reading = data.iloc[-1]
        measurement_delta = meter_current_reading - meter_previous_reading
        meter_reading_delta = correct_measurement_entry(meter_reading_delta,
                                                    measurement_delta)
        corrected_reading = meter_reading_delta * meter_multiplier
    elif measurement_unit.lower() == 'kwh':
        meter_previous_reading = data.iloc[0]
        meter_current_reading = data.iloc[-1]

    return {'meter_id': meter_id,
            'measurement_unit': measurement_unit,
            'meter_read_current': safe_rounding(meter_current_reading),
            'meter_read_previous': safe_rounding(meter_previous_reading),
            'meter_read_delta': safe_rounding(meter_reading_delta),
            'corrected_meter_value': safe_rounding(corrected_reading),
            'meter_reading_type': 'absolute',
            'interval_start': interval_start.date().strftime('%Y-%m-%d'),
            'interval_end': interval_end.date().strftime('%Y-%m-%d'),
            'measurement_type': measurement_type,
            'measurement_uuid': measurement_uuid
            }, corrected_reading


def prepare_charge_entry(rule, charge, start_date, end_date,
                         report_cache, meter_id, total_measurement,
                         measurement_uuid_cache={},
                         group_type_cache=None, parent_id=None
                         ,measurement_uuid=None):
    rate_unit = '['+ rule['rate_unit']+']'
    rate = '[' + rule['rate_unit']+']'
    # print('what is charge', charge)
    if '/' in rule['rate_unit']:
        rule_rate_unit = rule['rate_unit'].split('/')[1]
    else:
        rule_rate_unit = rule['rate_unit']

    if parent_id is not None:
        try:
            measurement_uuid = measurement_uuid_cache[parent_id]
            measurement_uuid_cache[rule['_id']] = measurement_uuid
        except KeyError:
            return report_cache
    else:
            measurement_uuid_cache[rule['_id']] = measurement_uuid

    if rule_rate_unit.lower() == 'kw':
        total_measurement = round(total_measurement, 3)
        group_name = 'demand'
        formula_type = 'total_demand'
    elif rule_rate_unit.lower() == 'kwh':
        total_measurement = round(total_measurement, 3)
        group_name = 'consumption'
        formula_type = 'total_consumption'

    elif rule['calculated_on'].lower() != 'measurement' and pd.notna(rule['tax_type']) \
            and ''.join(rule['charge_type'].split()) != 'AdminFee':
        group_name = rule['tax_type']
        formula_type = 'subtotal'
        rule_rate_unit = '$'
        rate = '[$/$]'
        total_measurement = round(total_measurement, 2)
    else:
        total_measurement = round(total_measurement, 2)
        group_name = 'other'
        if ''.join(rule['charge_type'].lower().split()) == 'adminfee':
            group_name = 'Admin Fee'
        if ''.join(rule['charge_type'].lower().split()) == 'servicefee':
            group_name = 'Service Fee'
        formula_type = 'subtotal'
        if rule['rate_unit'] == '%':
            rule_rate_unit = '$'
            rate = '[$/$]'
        else:
            rate = '['+rule['rate_unit'] +']'

    interval_start = start_date if start_date >= rule['start_date']\
                                else rule['start_date']
    interval_end = end_date if end_date <= rule['end_date']\
                            else rule['end_date']
    if parent_id is not None:
        charge_description = rule['charge_entry_' + str(parent_id)]
    else:
        charge_description = rule['charge_type']

    notes_rate = rule['value']
    if rule['rate_unit'] == '%':
        notes_rate = rule['value'] / 100

    record = {'meter_id': meter_id, 'name': charge_description,
                'group_name': group_name,
                'rate': rule['value'], 'rate_unit': rule['rate_unit'],
                'amount': float('{0:.2f}'.format(charge)),
                'start': interval_start, 'end': interval_end,
                'amount_currency': 'usd', 'measurement_uuid': measurement_uuid,
                'notes': formula_type + ' x ' + 'rate = total_amount',
                'rate_notes': str(total_measurement) +
                              ' [' + rule_rate_unit + '] x ' +
                              str(round(notes_rate, 6)) + rate
                              + ' = ' + str(round(charge, 2)) + '[$]'
                }
    report_cache.append(record)
    return report_cache


def include_measurement(measurements_list, test_measurement):
    for measurement in measurements_list:
        if all([measurement['meter_id']==test_measurement['meter_id'],
               measurement['measurement_unit']==test_measurement['measurement_unit'],
               measurement['meter_read_current']==test_measurement['meter_read_current'],
               measurement['meter_read_previous'] == test_measurement['meter_read_previous'],
               measurement['meter_read_delta'] == test_measurement['meter_read_delta'],
               measurement['interval_start'] == test_measurement['interval_start'],
               measurement['interval_end'] == test_measurement['interval_end'],
               measurement['measurement_type'] == test_measurement['measurement_type'],
               measurement['meter_read_previous'] == test_measurement['meter_read_previous']]
               ):
            return False, measurement['measurement_uuid']
    else:
        return True, None


def is_on_peak(rule):
    if pd.to_datetime('07:00').time() < pd.to_datetime(rule['start_time']).time() \
        < pd.to_datetime('12:00').time():
        return True
    else:
        return False


def save_report(company, building, account_number, statements, entity):
    for statement in statements:
        home = os.path.expanduser('~')
        dir_name = str(home + '/.rates_engine/' + company + '/' + building +
                       '/' + entity)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        file = open(os.path.join(dir_name, account_number + '.json'), 'w+')
        statement['company'] = company
        json.dump(statement, file)
        file.close()


def merge_off_peak_measurements(measurements_cache):
    meter_read_delta = 0
    corrected_meter_value = 0
    interval_start = measurements_cache[0]['interval_start']
    interval_end = measurements_cache[0]['interval_end']
    measurement_unit = None
    meter_read_current = None
    meter_read_previous = None
    measurement_uuid=None
    pop_list = []
    for measurement in measurements_cache:
        if measurement['measurement_type'] == 'electric_consumption_offpeak':
            meter_read_delta += measurement['meter_read_delta']
            corrected_meter_value += measurement['corrected_meter_value']
            measurement_uuid = measurement['measurement_uuid']
            measurement_unit = measurement['measurement_unit']
            meter_read_current = measurement['meter_read_current']
            meter_read_previous = measurement['meter_read_previous']
            pop_list.append(measurement)
    if len(pop_list) > 0:
        measurements_cache.append(
            {
                'meter_id': measurements_cache[0]['meter_id'],
                'measurement_unit': measurement_unit,
                'meter_read_current': float(meter_read_current),
                'meter_read_previous': float(meter_read_previous),
                'meter_read_delta': float(meter_read_delta),
                'corrected_meter_value': float(corrected_meter_value),
                'meter_reading_type': 'absoulte',
                'interval_start': interval_start,
                'interval_end': interval_end,
                'measurement_type': 'electric_consumption_offpeak',
                'measurement_uuid': measurement_uuid
            }
        )

        for item in pop_list:
            del measurements_cache[measurements_cache.index(item)]
    return measurements_cache, pop_list, measurement_uuid


def update_charges_with_merged_measurement(charge_report, deleted_uuids, new_uuid):
    update_rates_cache = {}
    delete_charges = []
    for id in deleted_uuids:
        update_rates_cache[id['measurement_uuid']] = list()
    deleted_ids = [item['measurement_uuid'] for item in deleted_uuids]
    for charge in charge_report:
        if charge['measurement_uuid'] in deleted_ids:
            update_rates_cache[charge['measurement_uuid']].append(charge)
            delete_charges.append(charge)
    merged_charges = merge_charges(update_rates_cache, new_uuid)
    for charge in delete_charges:
        del charge_report[charge_report.index(charge)]
    charge_report.extend(merged_charges)
    return charge_report


def merge_charges(charges, new_uuid):
    keys_list = list(charges.keys())
    rates_list = []
    already_seen = []
    for index1 in range(0, len(charges)-1):
        for test_charge in charges[keys_list[index1]]:
            rate_list = list()
            for index2 in range(1, len(charges)):
                if index2 == index1:
                    continue
                for current_charge in charges[keys_list[index2]]:
                    if index2 != index1 and \
                            current_charge['rate'] == test_charge['rate'] and \
                            current_charge['rate_unit'] == test_charge['rate_unit'] and \
                            current_charge['group_name'] == test_charge['group_name']:
                        if current_charge in already_seen or test_charge in already_seen:
                            continue
                        else:
                            already_seen.append(current_charge)
                            already_seen.append(test_charge)
                        if current_charge not in rate_list:
                            rate_list.append(current_charge)
                        if test_charge not in rate_list:
                            rate_list.append(test_charge)
                rates_list.append(rate_list)
    aggregated_charges = generate_aggregated_charge(charges_list=rates_list, new_uuid=new_uuid)
    return aggregated_charges


def generate_aggregated_charge(charges_list, new_uuid):
    new_rates = []
    for charge_list in charges_list:
        new_rates.append({
        'meter_id': charge_list[0]['meter_id'],
        'name': charge_list[0]['name'],
        'group_name': charge_list[0]['group_name'],
        'rate': charge_list[0]['rate'],
        'rate_unit': charge_list[0]['rate_unit'],
        'amount': round(sum([charge['amount'] for charge in charge_list]),2),
        'start': charge_list[0]['start'],
        'end': charge_list[0]['end'],
        'amount_currency': 'usd',
        'measurement_uuid': new_uuid,
        'notes': charge_list[0]['notes'],
        'rate_notes': merge_rate_notes([charge['rate_notes'] for charge in charge_list])
        })
    return new_rates


def merge_rate_notes(rate_notes):
    rate_note1 = rate_notes[0].split('=')[0]
    rate_notes_left = rate_note1.split('x')[0]
    rate_notes_right = rate_note1.split('x')[1]
    rate_unit = rate_notes_left.split('[')[1]
    charge_unit = rate_notes_right.split('[')[1]
    rate_note_charge = float(rate_notes_right.split('[')[0])
    rate_amount_list = []
    rate_measurement_list = []
    for rate_note in rate_notes:
        rate_amount = float(rate_note.split('=')[1].split('[')[0])
        rate_note_copy = rate_note.split('=')[0].split('x')
        rate_note_measurement = float(rate_note_copy[0].split('[')[0])
        rate_amount_list.append(rate_amount)
        rate_measurement_list.append(rate_note_measurement)
    total_charges = sum(rate_amount_list)
    total_measurements = sum(rate_measurement_list)
    if ('[' + rate_unit) == '[$]':
        total_measurements = round(total_measurements, 2)
    else:
        total_measurements = round(total_measurements, 3)
    charge_note = str(total_measurements) + '[' + rate_unit + 'x '\
                  + str(rate_note_charge) + '['+charge_unit+ ' = ' +\
                  str(round(total_charges, 2)) + '[$]'
    return charge_note


def find_distinct_meters(meters_doc):
    previous_seen = set()
    total_meters = 0
    for meter in meters_doc:
        if meter['meter_number'] not in previous_seen:
            total_meters += 1
            previous_seen.add(meter['meter_number'])
    return total_meters
