# coding=utf-8
from pip.download import PipSession
from pip.req import parse_requirements
from setuptools import setup, find_packages

install_requirements = parse_requirements('requirements.txt',
                                          session=PipSession())

requirements = [str(ir.req) for ir in install_requirements]
packages = find_packages(
    exclude=['​*.tests', '*​.tests.​*', 'tests.*​', 'tests']
)

setup(
    name='rates_engine',
    version='1.0.1',
    packages=packages,
    license='Proprietary',
    author='PrescriptiveData',
    author_email='gsingh@prescriptivedata.io',
    description='Rates engine to generate bill internally',
    install_requires=requirements,
    scripts=['bin/run_rates_engine']
)