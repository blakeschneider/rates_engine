import os
import sys

# force py.test to test only on development project, not installed module
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


