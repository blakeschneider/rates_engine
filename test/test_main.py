from unittest import mock
from unittest.mock import patch
import pandas as pd
import numpy as np

from rates_engine.main import generate_report_helper, generate_report


@patch('rates_engine.main.process_demand_rules')
@patch('rates_engine.main.process_consumption_rules')
@patch('rates_engine.main.prepare_measurement_description')
@patch('rates_engine.main.get_holiday_list')
def test_generate_report_helper_consumption(
        mock_holiday_list, mock_prepare_description, mock_process_consumption,
        mock_process_demand):
    demand = np.array([i * 10 for i in range(1, 200)])
    timeseries = pd.Series(demand, pd.date_range(start='2018-09-01',
                                                 periods=len(demand),
                                                 freq='30min'))

    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement', 'days_applicable': [0, 1, 2, 3],
             'start_time': '08:00', 'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'kwh',
             'time_window': '30min', 'value': 30, 'parent_rules': [],
             '_id': '43fdr34fdr4', 'charge_type': 'consumption'}

    rule2 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'kwh',
             'time_window': '30min', 'value': 30, 'parent_rules': [],
             '_id': '43fdr34fdr4', 'charge_type': 'consumption'}
    rules = pd.DataFrame([rule1, rule2])

    mock_holiday_list.side_effect = mock.Mock(return_value=[])
    mock_prepare_description.side_effect = mock.Mock(
                                            return_value='consumption charges')
    mock_process_consumption.side_effect = mock.Mock(return_value=[10, 'c'])
    mock_process_demand.side_effect = mock.Mock(return_value=[10, 'd'])
    total_cost, report_cache, measurement_list = generate_report_helper(
        rules=rules, start_date='2018-09-01', end_date='2018-09-02',
        data=timeseries, mode='consumption', meter_id='jijijij',
        meter_multiplier=1.5)
    assert total_cost == 20
    assert len(measurement_list) == 2
    assert measurement_list[0] == 'c'
    assert measurement_list[1] == 'c'





@patch('rates_engine.main.process_subtotal_child_rules')
@patch('rates_engine.main.process_measurement_children')
@patch('rates_engine.main.process_demand_rules')
@patch('rates_engine.main.process_consumption_rules')
@patch('rates_engine.main.prepare_measurement_description')
@patch('rates_engine.main.get_holiday_list')
def test_generate_report_helper_with_child_rules(
        mock_holiday_list, mock_prepare_description, mock_process_consumption,
        mock_process_demand, mock_measurement_children,
        mock_subtotal_children):
    demand = np.array([i * 10 for i in range(1, 200)])
    timeseries = pd.Series(demand, pd.date_range(start='2018-09-01',
                                                 periods=len(demand),
                                                 freq='30min'))

    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement', 'days_applicable': [0, 1, 2, 3],
             'start_time': '08:00', 'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'kw',
             'time_window': '30min', 'value': 30, 'parent_rules': [],
             '_id': '43fdr34fdr4', 'charge_type': 'demand'}

    rule2 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'kw',
             'time_window': '30min', 'value': 30, 'parent_rules': [],
             '_id': '43fdr34fdr4', 'charge_type': 'demand'}

    rule3 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement', 'days_applicable': [0, 1, 2, 3],
             'start_time': '08:00', 'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'kwh',
             'time_window': '30min', 'value': 30, 'parent_rules': [1, 2],
             '_id': '43fdr34fdr4', 'charge_type': 'demand'}

    rule4 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'subtotal',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'per_rs',
             'time_window': '30min', 'value': 30, 'parent_rules': [3],
             '_id': '43fdr34fdr4', 'charge_type': 'demand'}

    rules = []
    rules.append([])
    rules.append(pd.DataFrame([rule1, rule2, rule3, rule4]))

    mock_holiday_list.side_effect = mock.Mock(return_value=[])
    mock_prepare_description.side_effect = mock.Mock(
                                            return_value='consumption charges')
    mock_process_consumption.side_effect = mock.Mock(return_value=[10, 'c'])
    mock_process_demand.side_effect = mock.Mock(return_value=[10, 'd'])
    mock_measurement_children.side_effect = mock.Mock(return_value=5)
    mock_subtotal_children.side_effect = mock.Mock(return_value=5)

    total_cost, report_cache, measurement_list = generate_report_helper(
        rules=rules, start_date='2018-09-01', end_date='2018-09-02',
        data=timeseries, mode='demand', meter_id='jijijij',
        meter_multiplier=1.5)
    assert total_cost == 30
    assert len(measurement_list) == 2
    assert measurement_list[0] == 'd'
    assert measurement_list[1] == 'd'


@patch('rates_engine.main.process_cumulative_rules')
@patch('rates_engine.main.filter_rules')
@patch('rates_engine.main.prepare_statement')
@patch('rates_engine.main.classify_rules')
@patch('rates_engine.main.generate_report_helper')
@patch('rates_engine.main.get_demand_data')
@patch('rates_engine.main.get_consumption_data')
@patch('rates_engine.main.get_billing_rules')
@patch('rates_engine.main.get_billing_meters')
@patch('rates_engine.main.get_billing_account')
@patch('rates_engine.main.get_billing_entity_id')
def test_generate_report(
        mock_billing_entity, mock_billing_account, mock_billing_meters,
        mock_billing_rules, mock_consumption_data, mock_demand_data,
        mock_generate_report_helper, mock_classify_rules,
        mock_prepare_statement, mock_filter_rules, mock_cumulative_charges):

    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement', 'days_applicable': [0, 1, 2, 3],
             'start_time': '08:00', 'floor_limit': -1,
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True,
             'rate_unit': 'kw', 'time_window': '30min', 'value': 30,
             'parent_rules': [], '_id': '43fdr34fdr4', 'charge_type': 'demand'}

    rule2 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'measurement',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True,
             'rate_unit': 'kwh', 'time_window': '30min', 'value': 30,
             'parent_rules': [], '_id': '43fdr34fdr4',
             'charge_type': 'consumption', 'floor_limit': 10}
    rule3 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'constant', 'days_applicable': [0, 1, 2, 3],
             'start_time': '08:00', 'floor_limit': 20, 'end_time': '12:00',
             'ceiling_limit': 50, 'holidays_applicable': True,
             'rate_unit': 'per_month', 'time_window': '30min', 'value': 30,
             'parent_rules': [], '_id': '43fdr34fdr4',
             'charge_type': 'monthly charge'}

    rule4 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'calculated_on': 'constant',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceiling_limit': 50,
             'holidays_applicable': True, 'rate_unit': 'per_bill',
             'time_window': '30min', 'value': 30, 'parent_rules': [],
             '_id': '43fdr34fdr4', 'charge_type': 'billing charge',
             'floor_limit': 30}

    x = np.array([i * 10 for i in range(1, 200)])
    consumption = pd.Series(x, pd.date_range(start='2018-09-01',
                                             periods=len(x), freq='30min'))

    consumption = consumption.to_frame()
    consumption.columns = ['meter1']
    x = np.array([1 * 10 for i in range(1, 200)])
    demand = pd.Series(x, pd.date_range(start='2018-09-01', periods=len(x),
                                        freq='30min'))
    demand = demand.to_frame()
    demand.columns = ['meter2']
    rules = [rule1, rule2, rule3, rule4]
    submeter_rules = pd.DataFrame([rule1, rule2])
    cumulative_rules = pd.DataFrame([rule3, rule4])

    meter1 = {'meter_number': 1, 'sensor_id': 'meter1', 'meter_multiplier': 1,
              'meter_id': 'meter1_id'}

    meter2 = {'meter_number': 2, 'sensor_id': 'meter2', 'meter_multiplier': 1,
              'meter_id': 'meter2_id'}

    mock_billing_rules.side_effect = mock.Mock(return_value=rules)
    mock_filter_rules.side_effect = mock.Mock(return_value=pd.DataFrame(rules))
    mock_classify_rules.side_effect = mock.Mock(return_value=[submeter_rules,
                                                              cumulative_rules]
                                                )
    mock_billing_entity.side_effect = mock.Mock(return_value='123456')
    mock_billing_account.side_effect = mock.Mock(return_value='12')
    mock_billing_meters.side_effect =\
                        mock.Mock(return_value=[meter1, meter2])
    mock_consumption_data.side_effect = mock.Mock(return_value=consumption)
    mock_demand_data.side_effect = mock.Mock(return_value=demand)
    mock_generate_report_helper.side_effect =\
                        mock.Mock(return_value=[10, 'cache', 'measurement'])
    mock_prepare_statement.side_effect = mock.Mock(return_value=None)
    mock_cumulative_charges.side_effect = mock.Mock(return_value=[20, []])
    total_charges, measurement = generate_report(
                        company='rudin', building='345_Park', provider='coned',
                        start_date='2017-01-19', end_date='2017-01-29',
                        zipcode=11211, schedule='large',
                        consumption_gran='30min', demand_gran='10min',
                        cache=False, use_beta=False)
    assert total_charges == 30