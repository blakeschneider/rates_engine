from datetime import date
from unittest.mock import MagicMock
from unittest import mock
from unittest.mock import patch
import pandas as pd
import numpy as np

from rates_engine.rates_logic import calculate_charge,\
    chunk_data_with_rule, calculate_consumption, demand_calculation_logic,\
    process_ceil_exceeding_chunk, validate_chunk


def test_calculate_charge():
    result = calculate_charge(1, 10)
    assert result == 10


def test_chunk_data_with_rule():
    demand = np.array([i * 10 for i in range(1, 100)])
    asserted_result = [170, 180, 190, 200, 210, 220, 230, 240, 250,
                                 650, 660, 670, 680, 690, 700, 710, 720, 730]
    timeseries = pd.Series(demand, pd.date_range(start='2018-09-01',
                            periods=len(demand), freq='30min'))
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
            'start_time': '08:00', 'end_time': '12:00',
             'floor_limit': -1}
    chunks, date_list = chunk_data_with_rule(timeseries, rule1, pointer_map={})
    print(chunks)
    assert list(chunks.values) == asserted_result
    assert list(date_list) == [date(2018, 9, 1), date(2018, 9, 2)]


def test_process_ceil_exceeding_chunk():
    # demand can stay constant over time
    demand = [1 * 10 for i in range(1, 20)]
    timeseries = pd.Series(
        demand, pd.date_range(start='2018-09-01', periods=len(demand),
                              freq='30min'))
    total_consumption = 100
    ceil_limit = 130
    start_datetime = '2018-09-01 00:00'
    mode1 = 'consumption_from_demand'
    mode2 = 'consumption_from_consumption'
    sum, pointer = process_ceil_exceeding_chunk(
        total_consumption, timeseries, ceil_limit, start_datetime, mode1)

    assert sum == 30
    assert str(pointer) == '2018-09-01 01:30:00'

    # consumption readings will be increasing over time.
    consumption = [i * 10 for i in range(1, 20)]
    timeseries = pd.Series(
        consumption, pd.date_range(start='2018-09-01',
                                   periods=len(consumption), freq='30min'))
    sum, pointer = process_ceil_exceeding_chunk(
        total_consumption, timeseries, ceil_limit, start_datetime, mode2)

    assert sum == 30
    assert str(pointer) == '2018-09-01 01:30:00'


def test_validate_chunk_none_start_and_end_in_range():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': True}
    start_datetime = '2018-09-01 08:00'
    end_datetime = '2018-10-01 11:00'
    holidays_list = []

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid == False
    assert start == 0
    assert end == 0


def test_validate_chunk_both_start_and_end_in_range():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': True,
             'months_applicable': [5, 6, 7, 8, 9]}
    start_datetime = '2018-07-01 08:00'
    end_datetime = '2018-08-01 11:00'
    holidays_list = []

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid == True
    assert start == pd.to_datetime(start_datetime)
    assert end == pd.to_datetime(end_datetime)


def test_validate_chunk_only_start_in_range():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0,1, 2, 3, 4], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': True,
             'months_applicable': [5, 6, 7, 8, 9]}
    start_datetime = '2018-06-29 08:00'
    end_datetime = '2018-08-05 22:00'
    holidays_list = []

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid is True
    assert start == pd.to_datetime(start_datetime)
    assert pd.to_datetime(end) == pd.to_datetime('2018-08-05 00:00')


def test_validate_chunk_only_end_in_range():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0,1, 2, 3,6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': True,
             'months_applicable': [5, 6, 7, 8, 9]}
    start_datetime = '2018-06-29 08:00'
    end_datetime = '2018-08-05 22:00'
    holidays_list = []

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid is True
    assert pd.to_datetime(start) == pd.to_datetime('2018-06-29 23:59')


def test_validate_chunk_holidays_applicable():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0,1, 2, 3,5,4,6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': True,
             'months_applicable': [5,6, 7, 8, 9]}
    start_datetime = '2018-06-29 08:00'
    end_datetime = '2018-08-05 22:00'
    holidays_list = []

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid is True
    assert pd.to_datetime(start) == pd.to_datetime(start_datetime)
    assert pd.to_datetime(end) == pd.to_datetime(end_datetime)


def test_validate_chunk_holidays_not_applicable_start_date():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0,1, 2, 3,5,4,6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': False,
             'months_applicable': [6, 7, 8, 9]}
    start_datetime = '2018-06-29 08:00'
    end_datetime = '2018-08-05 22:00'
    holidays_list = [pd.to_datetime('2018-06-29').date()]

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid is True
    assert pd.to_datetime(start) == pd.to_datetime('2018-06-29 00:00')
    assert pd.to_datetime(end) == pd.to_datetime(end_datetime)


def test_validate_chunk_rules_not_applicable_end_date():
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0,1, 2, 3,5,4,6], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': False,
             'months_applicable':[6, 7, 8, 9]}
    start_datetime = '2018-06-29 08:00'
    end_datetime = '2018-08-05 22:00'
    holidays_list = [pd.to_datetime('2018-08-05').date()]

    valid, start, end = validate_chunk(
        rule1, start_datetime, end_datetime, holidays_list)
    assert valid is True
    assert pd.to_datetime(end) == pd.to_datetime('2018-08-05 00:00')


@patch('rates_engine.rates_logic.process_ceil_exceeding_chunk')
@patch('rates_engine.rates_logic.validate_chunk')
def test_calculate_consumption_return_True(
        mock_validate_chunk, mock_process_ceil_exceeding_chunk):
    x= [1*10 for i in range(1, 200)]
    chunked_data = pd.Series(x, pd.date_range(start='2018-09-01',
                                              periods=len(x), freq='30min'))
    holidays_list1 = []
    mock_process_ceil_exceeding_chunk.side_effect = mock.Mock(return_value=[0, 0])
    mock_validate_chunk.side_effect =\
        mock.Mock(return_value=[True, '2018-09-01 08:00', '2018-09-01 10:30'])
    chunked_dates = [date(2018, 9, 1)]
    pointer_map = {}
    calculation_type = 'consumption_from_demand'
    rule1 = {'start_date':'2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0, 1, 2, 3, 4, 5, 6], 'start_time': '08:00',
             'end_time': '12:00', 'ceil_limit': 90, 'floor_limit': -1,
             'ceiling_limit': -1,
             'rate_unit':'kw', 'window':'30min',
             'value':30, '_id':'43fdr34fdr4', 'charge_type':'demand'}

    total_consumption = calculate_consumption(
        chunked_data, chunked_dates, rule1, holidays_list1, pointer_map,
        calculation_type)
    assert total_consumption == 50
