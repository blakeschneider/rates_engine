import unittest
from unittest import mock
from unittest.mock import patch
import pandas as pd

from rates_engine.rules_handler import process_consumption_rules

from rates_engine.rules_handler import classify_rules, filter_rules,\
                process_demand_rules, process_measurement_children,\
                process_subtotal_child_rules, process_constant_rules,\
                process_monthly_rules, process_total_child_rules


def test_classify_rules():
    rule1 = {'calculated_on': 'constant'}
    rule2 = {'calculated_on': 'total'}
    rule3 = {'calculated_on': 'measurement'}

    rules_list = [rule1, rule2, rule3]
    submeter_rules, cumulative_rules = classify_rules(
                                        pd.DataFrame(rules_list, index=None))
    for _, rule in submeter_rules.iterrows():
        assert rule['calculated_on'] == rule3['calculated_on']

    for _, rule in cumulative_rules.iterrows():
        assert rule['calculated_on'] in ['total', 'constant']


def test_filter_rules_valid_rules():

    start_date = '2017-08-01'
    end_date = '2017-09-01'
    rule1 = {'start_date': '2017-07-01', 'end_date': '2017-08-20'}
    rule2 = {'start_date': '2017-08-20', 'end_date': '2017-09-03'}
    rule3 = {'start_date': '2017-06-01', 'end_date': '2017-09-30'}
    rule4 = {'start_date': '2017-08-15', 'end_date': '2017-08-15'}

    rules_df = filter_rules(pd.DataFrame([rule1, rule2, rule3, rule4]),
                            start_date, end_date)
    assert len(rules_df) == 4


def test_filter_rules_invalid_rules():
    start_date = '2017-08-01'
    end_date = '2017-09-01'
    rule1 = {'start_date': '2017-07-01', 'end_date': '2017-07-20'}
    rule2 = {'start_date': '2017-09-03', 'end_date': '2017-09-20'}

    rules_df = filter_rules(pd.DataFrame([rule1, rule2]),
                            start_date, end_date)
    assert len(rules_df) == 0


@patch('rates_engine.rules_handler.chunk_data_with_rule')
@patch('rates_engine.rules_handler.calculate_consumption')
@patch('rates_engine.rules_handler.calculate_charge')
@patch('rates_engine.rules_handler.save_subtotal')
@patch('rates_engine.rules_handler.save_measurement')
@patch('rates_engine.rules_handler.prepare_charge_entry')
@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.validate_chunk')
def test_process_consumption_rules(
        mock_validate, mock_measurement_entry, mock_charge_entry,
        mock_save_measurement, mock_save_subtotal, mock_calculate_charge,
        mock_calculate_consumption,
        mock_chunk_data):
    rule = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
            'days_applicable': [0, 1, 2, 3, 5, 4, 6], 'start_time': '08:00',
            'end_time': '12:00', 'holidays_applicable': False,
            'value': 20, '_id': '43d4rf4', 'rate_unit': 'kw',
            'calculated_on': 'demand'}
    mock_chunk_data.side_effect = \
        mock.Mock(return_value=[[1, 2, 3], ['2018-09-01 08:00']])
    mock_calculate_consumption.side_effect = \
        mock.Mock(return_value=10)
    mock_calculate_charge.side_effect = \
        mock.Mock(return_value=20)
    mock_save_subtotal.side_effect = \
        mock.Mock(return_value=None)
    mock_save_measurement.side_effect = \
        mock.Mock(return_value=None)
    mock_charge_entry.side_effect = \
        mock.Mock(return_value=None)
    mock_measurement_entry.side_effect = \
        mock.Mock(return_value=None)
    mock_validate.side_effect = \
        mock.Mock(return_value=[True, '2018-09-01 08:00', '2018-09-01 10:30'])

    total_charges, measurement_entry = process_consumption_rules(
        rule, pd.DataFrame({}), {}, {},
        {}, [], {}, '2018-09-01', '2018-09-05',
        'jdkdkdkjfd', 1.5, 'consumption_from_demand')

    assert total_charges == 20
    assert measurement_entry is None


@patch('rates_engine.rules_handler.chunk_data_with_rule')
@patch('rates_engine.rules_handler.calculate_consumption')
@patch('rates_engine.rules_handler.calculate_charge')
@patch('rates_engine.rules_handler.save_subtotal')
@patch('rates_engine.rules_handler.save_measurement')
@patch('rates_engine.rules_handler.prepare_charge_entry')
@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.validate_chunk')
def test_process_demand_rules(
        mock_validate, mock_measurement_entry, mock_charge_entry,
        mock_save_measurement, mock_save_subtotal, mock_calculate_charge,
        mock_calculate_consumption,
        mock_chunk_data):
    rule = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
            'days_applicable': [0, 1, 2, 3, 5, 4, 6], 'start_time': '08:00',
            'end_time': '12:00', 'holidays_applicable': False,
            'value': 6, '_id': '43d4rf4', 'rate_unit': 'kw',
            'calculated_on':'demand'}
    x = [1 * 10 for i in range(1, 200)]
    chunked_data = pd.Series(x, pd.date_range(start='2018-09-01',
                                              periods=len(x), freq='30min'))

    mock_chunk_data.side_effect = \
        mock.Mock(return_value=[chunked_data, [pd.to_datetime('2018-09-01')]])
    mock_calculate_consumption.side_effect = \
        mock.Mock(return_value=10)
    mock_calculate_charge.side_effect = \
        mock.Mock(return_value=20)
    mock_save_subtotal.side_effect = \
        mock.Mock(return_value=None)
    mock_save_measurement.side_effect = \
        mock.Mock(return_value=None)
    mock_charge_entry.side_effect = \
        mock.Mock(return_value=None)
    mock_measurement_entry.side_effect = \
        mock.Mock(return_value=None)
    mock_validate.side_effect = \
        mock.Mock(return_value=[True, '2018-09-01 08:00', '2018-09-01 10:30'])

    total_charges, measurement_entry = process_demand_rules(
        rule, pd.DataFrame({}), '30min', {},
        {}, {}, [],
        {}, '2018-09-01', '2018-09-01',
        'jfdj8dfhd8', 1.0)
    assert total_charges == 20
    assert measurement_entry is None


@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.calculate_charge')
@patch('rates_engine.rules_handler.retrieve_measurement')
def test_process_measurement_children(
        mock_retrieve_measurement, mock_calculate_charge,
        mock_prepare_measurement_entry):
    measurement_entry = [{'rule_id': '123456', 'total': 10},
                         {'rule_id': 'abcd', 'total': 20}]
    rules = [{'value': 6, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['123456', 'abcd'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'measurement'}]
    mock_retrieve_measurement.side_effect = mock.Mock(
                                            return_value=measurement_entry)
    mock_calculate_charge.side_effect = mock.Mock(return_value=60)
    mock_prepare_measurement_entry.side_effect = mock.Mock(return_value=None)

    total = process_measurement_children(rules, {}, [],
                                         '2017-08-01', '2017-09-01', '121212')

    assert total == 60


@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.calculate_charge')
@patch('rates_engine.rules_handler.retrieve_subtotal')
def test_process_subtotal_children(
        mock_retrieve_subtotal, mock_calculate_charge, mock_prepare_entry):

    mock_retrieve_subtotal.side_effect = mock.Mock(return_value=10)
    mock_calculate_charge.side_effect = mock.Mock(return_value=10)
    mock_prepare_entry.side_effect = mock.Mock(return_value=None)

    rules = [{'value': 6, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['123456', 'abcd'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on': 'subtotal'},
             {'value': 12, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['535353', 'bhbhbh'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'subtotal'}
             ]
    total = process_subtotal_child_rules(rules, {}, [], '2017-08-19',
                                         '2017-08-19', 'huhududud')

    assert total == 40


@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.save_subtotal')
@patch('rates_engine.rules_handler.calculate_charge')
def test_process_constant_rules(
        mock_calculate_charge, mock_save_subtotal, mock_prepare_entry):

    mock_calculate_charge.side_effect = mock.Mock(return_value=10)
    mock_save_subtotal.side_effect = mock.Mock(return_value=None)
    mock_prepare_entry.side_effect = mock.Mock(return_value=None)

    rules = [[1, {'value': 6, '_id': '43d4rf4', 'rate_unit': 'per_meter',
                  'parent_rules': ['123456', 'abcd'],
                  'start_date': '2017-08-19', 'end_date': '2018-08-19',
                  'charge_type': 'constatnt', 'calculated_on':'subtotal'}],
             [2, {'value': 12, '_id': '43d4rf4', 'rate_unit': 'kw',
                  'parent_rules': ['535353', 'bhbhbh'],
                  'start_date': '2017-08-19',
                  'end_date': '2018-08-19', 'charge_type': 'consumption',
                  'calculated_on': 'subtotal'}]
             ]

    total = process_constant_rules({}, rules, [], '2017-08-19', '2017-08-19',
                                   'yy775h585j8')

    assert total == 20


@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.save_subtotal')
def test_process_monthly_rules(mock_save_subtotal, mock_prepare_entry):

    mock_save_subtotal.side_effect = mock.Mock(return_value=None)
    mock_prepare_entry.side_effect = mock.Mock(return_value=None)
    rules = [{'value': 6, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['123456', 'abcd'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'consumption'},
             {'value': 12, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['535353', 'bhbhbh'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'consumption'}
             ]
    total1 = process_monthly_rules(rules, {}, '2017-08-01', '2017-09-01', [],
                                   'yy775h585j8')
    total2 = process_monthly_rules(rules, {}, '2017-08-01', '2017-10-01', [],
                                   'yy775h585j8')
    assert total1 == 18
    assert total2 == 36


@patch('rates_engine.rules_handler.prepare_measurement_entry')
@patch('rates_engine.rules_handler.save_subtotal')
@patch('rates_engine.rules_handler.calculate_charge')
def test_process_total_child_rules(mock_calculate_charge, mock_save_subtotal,
                                   mock_prepare_entry):

    mock_save_subtotal.side_effect = mock.Mock(return_value=None)
    mock_prepare_entry.side_effect = mock.Mock(return_value=None)
    rules = [{'value': 6, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['123456', 'abcd'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'consumption'},
             {'value': 12, '_id': '43d4rf4', 'rate_unit': 'kw',
              'parent_rules': ['535353', 'bhbhbh'], 'start_date': '2017-08-19',
              'end_date': '2018-08-19', 'charge_type': 'consumption',
              'calculated_on':'consumption'}
             ]
    total_charges = 100
    mock_calculate_charge.side_effect = mock.Mock(return_value=100)
    total = process_total_child_rules(
                rules, total_charges, [], '2017-08-01', '2017-09-01',
                'yy775h585j8')
    assert total == 200


if __name__ == '__main__':
    unittest.main()
