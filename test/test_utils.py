import pandas as pd
import numpy as np

from rates_engine.utils import prepare_measurement_entry, prepare_charge_entry


def test_prepare_measurement_entry_type_demand():
    demand = np.array([i * 10 for i in range(1, 100)])
    timeseries = pd.Series(demand, pd.date_range(start='2017-08-01',
                                                 periods=len(demand),
                                                 freq='60min'))
    rule1 = {'start_date': '2017-08-19', 'end_date': '2018-08-19',
             'days_applicable': [0, 1, 2, 3, 4], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': False,
             'rate_unit': 'kw'}

    result = prepare_measurement_entry(
        meter_id='12', measurement_unit='kw', meter_reading_delta=2,
        corrected_reading=2, start_date='2017-08-01',
        end_date='2017-08-02',
        rule=rule1, measurement_type='demand', data=timeseries)

    assert result['meter_id'] == '12'
    assert result['measurement_unit'] == 'kw'
    assert result['meter_read_current'] == 2
    assert result['meter_read_previous'] == 0
    assert result['meter_read_delta'] == 2
    assert result['corrected_meter_value'] == 2
    assert result['meter_reading_type'] == 'demand'
    assert result['interval_start'] == '2017-08-19'
    assert result['interval_end'] == '2017-08-02'
    assert result['measurement_type'] == 'demand'


def test_prepare_measurement_entry_type_consumption():
    demand = np.array([i * 10 for i in range(1, 100)])
    timeseries = pd.Series(demand, pd.date_range(start='2017-08-01',
                                                 periods=len(demand),
                                                 freq='60min'))
    rule1 = {'start_date': '2017-08-01', 'end_date': '2017-08-04',
             'days_applicable': [0, 1, 2, 3, 4], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': False,
             'rate_unit': 'kwh', 'calculated_on':'consumption',
             'floor_limit':-1}

    result = prepare_measurement_entry(
        meter_id='12', measurement_unit='kwh', meter_reading_delta=2,
        corrected_reading=70, start_date='2017-08-01',
        end_date='2017-08-07',
        rule=rule1, measurement_type='consumption', data=timeseries)

    assert result['meter_id'] == '12'
    assert result['measurement_unit'] == 'kwh'
    assert result['meter_read_current'] == 850
    assert result['meter_read_previous'] == 90
    assert result['meter_read_delta'] == 2
    assert result['corrected_meter_value'] == 70
    assert result['meter_reading_type'] == 'consumption'
    assert result['interval_start'] == '2017-08-01'
    assert result['interval_end'] == '2017-08-04'
    assert result['measurement_type'] == 'consumption'


def test_prepare_charge_entry():
    rule1 = {'value': 10, 'start_date': '2017-08-01', 'end_date': '2017-08-04',
             'days_applicable': [0, 1, 2, 3, 4], 'start_time': '08:00',
             'end_time': '12:00', 'holidays_applicable': False,
             'rate_unit': 'kwh', 'charge_type': 'Billing and payment charges',
             'calculated_on': 'measurement', 'floor_limit': -1}
    result = prepare_charge_entry(
        rule=rule1, charge=50, start_date='2017-08-01', end_date='2017-08-07',
        report_cache=[], meter_id='12', total_measurement=20)[0]

    assert result['meter_id'] == '12'
    assert result['name'] == rule1['charge_type']
    assert result['group_name'] == 'consumption'
    assert result['rate'] == rule1['value']
    assert result['rate_unit'] == rule1['rate_unit']
    assert result['amount'] == 50.0
    assert result['start'] == '2017-08-01'
    assert result['end'] == '2017-08-04'
    assert result['amount_currency'] == 'usd'
    assert result['measurement_type'] == 'consumption'
